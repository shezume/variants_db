CREATE DATABASE Variants_DB;

use Variants_DB;

DROP table IF EXISTS sampleVariant;
DROP table IF EXISTS sample;
DROP table IF EXISTS variant;
DROP table IF EXISTS patient;



CREATE TABLE variant (
    id_var INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    chrom VARCHAR(5) NOT NULL,
    posStart INT NOT NULL, 
    posStop INT,
    ref TEXT DEFAULT NULL,
    alt TEXT DEFAULT NULL,
    info TEXT DEFAULT NULL ,
    type ENUM ('SNV', 'CNV'),
    subtype ENUM ('INS', 'DEL', 'DUP', 'SNV') DEFAULT NULL,
    is_coding BOOLEAN DEFAULT NULL,
    is_exonic BOOLEAN DEFAULT NULL,
    is_lof BOOLEAN DEFAULT NULL,
    exon VARCHAR(100)   
) ENGINE = InnoDB;

CREATE INDEX index_chrom_posStart ON variant (chrom, posStart); 

CREATE TABLE pharmacoAnotation (
  id_entry INT PRIMARY KEY NOT NULL,
  chrom VARCHAR(5) NOT NULL,
  posStart INT NOT NULL,
  posStop INT NOT NULL,
  ref VARCHAR(200) NOT NULL,
  source_ori VARCHAR(50) NOT NULL,
  last_maj DATE,
  quality VARCHAR(10),
  id_var INT,
  info VARCHAR (200)
) ENGINE = InnoDB;


CREATE INDEX variant_chr_idx on variant(chrom);

CREATE TABLE project (
  id_project VARCHAR(30) PRIMARY KEY NOT NULL,
  type_project ENUM ('TSO', 'CES', 'WES'),
  date_proj DATE
) ENGINE = InnoDB;

CREATE TABLE groupDisease (
  id_groupe_disease INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  short_name VARCHAR(50) NOT NULL
) ENGINE = InnoDB;

CREATE TABLE disease (
  id_disease INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  id_groupe_disease INT,
  tag VARCHAR(50) NOT NULL,
  name VARCHAR(50) NOT NULL,
  inheritance varchar(20)
) ENGINE = InnoDB;


CREATE INDEX disease_tag_idx on disease(tag);

ALTER TABLE disease
ADD FOREIGN KEY (id_groupe_disease) REFERENCES groupDisease(id_groupe_disease); 

CREATE TABLE patient (
    id_patient INT PRIMARY KEY NOT NULL,
    age INT(3) default NULL,
    weight FLOAT default NULL,
    height FLOAT default NULL,
    gender ENUM('F','M') default NULL
    ) ENGINE = InnoDB;
    
CREATE TABLE family (
    id_family VARCHAR(40) PRIMARY KEY NOT NULL,
    tag VARCHAR(50)  NOT NULL   
    ) ENGINE = InnoDB;
    
    
CREATE INDEX family_tag_idx on family(tag);

CREATE TABLE familyPatient (
    id_family VARCHAR(40) not null,
    id_patient INT NOT NULL,
    probandus BOOLEAN,
    PRIMARY KEY (id_patient, id_family)
    ) ENGINE = InnoDB;



CREATE TABLE patientComorbility (   
    id_patient INT NOT NULL,   
    type_comorb ENUM ('ANAEMIA', 'CARDIO', 'PULMONARY', 'DIABETES' ,'RENAL',
'NEURO', 'ONCO', 'HTA'),   
    value_comorb int default null,   
    PRIMARY KEY (id_patient, type_comorb),   
    FOREIGN KEY (id_patient) REFERENCES patient(id_patient) ON DELETE CASCADE ) ENGINE = InnoDB;


CREATE TABLE sample (
    id_sample VARCHAR(7) PRIMARY KEY NOT NULL,
    id_project VARCHAR(30)  DEFAULT NULL,
    id_patient INT  DEFAULT NULL,
    status BOOLEAN DEFAULT FALSE,
    FOREIGN KEY (id_project) REFERENCES project(id_project) ON DELETE CASCADE,
    FOREIGN KEY (id_patient) REFERENCES patient(id_patient) ON DELETE CASCADE
    ) ENGINE = InnoDB;

# allele_type 2=hom 1=het 0=none 1=deepKO

CREATE TABLE sampleVariant (
    id_var INT NOT NULL,
    id_sample VARCHAR(7) NOT NULL,
    genotype ENUM ('HOM','HET', 'HC','REF','NONE') not NULL,
    genotype_count INT(1),
    gt_depths INT DEFAULT NULL,
    PRIMARY KEY (id_sample, id_var)
) ENGINE = InnoDB;

CREATE INDEX idx_idvar on sampleVariant(id_var);
CREATE INDEX idx_idsample on sampleVariant(id_sample);



CREATE TABLE drugs (
  id_drug INT PRIMARY KEY AUTO_INCREMENT,
  drug_name VARCHAR(50) NOT NULL
) ENGINE = InnoDB;

CREATE TABLE patientDrugs (
  id_patient INT,
  id_drug INT,
  PRIMARY KEY (id_patient, id_drug),
  FOREIGN KEY (id_drug) REFERENCES drugs(id_drug) ON DELETE CASCADE,
  FOREIGN KEY (id_patient) REFERENCES patient(id_patient) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE patientDrugs2 (
  id_patient INT,
  id_drug INT,
  treatment_start DATE DEFAULT NULL,
  treatment_end DATE DEFAULT NULL,
  PRIMARY KEY (id_patient, id_drug, treatment_start),
  FOREIGN KEY (id_drug) REFERENCES drugs(id_drug) ON DELETE CASCADE,
  FOREIGN KEY (id_patient) REFERENCES patient(id_patient) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE analysis (
    id_patient INT NOT NULL,
    date_analysis DATE,
    exitus BOOLEAN NOT NULL,
    uci  BOOLEAN NOT NULL,
    type ENUM ('H', 'U', 'C'),
    Microalbuminuria DECIMAL default null,
    plasm_ammonia DECIMAL default null,
    base_excess DECIMAL default null,
    bilirubin DECIMAL default null,
    urea_Nitrogen DECIMAL default null,
    creatin_Kinase  DECIMAL default null,
    CK_MB  DECIMAL default null,
    Creatinine  DECIMAL default null,
    D_Dimer  DECIMAL default null,
    Ferritin  DECIMAL default null,
    GGT  DECIMAL default null,
    Glucose  DECIMAL default null,
    ASAT_GOT  DECIMAL default null,
    ALAT_GPT  DECIMAL default null,
    Hepatitis_B DECIMAL default null,
    Bicarbonate  DECIMAL default null,
    Erythrocytes DECIMAL default null,
    Hemoglobine DECIMAL default null,
    INR DECIMAL default null,
    Lactate DECIMAL default null,
    LDH DECIMAL default null,
    Leucocites DECIMAL default null,
    Lynphocytes_Perc DECIMAL default null,
    Lynphocytes_Abs DECIMAL default null,
    Monocytes_Perc DECIMAL default null,
    Monocytes_Abs DECIMAL default null,
    Na DECIMAL default null,
    PCO2 DECIMAL default null,
    Reactive_C_Protein  DECIMAL default null,
    Ph  DECIMAL default null,
    Platelet  DECIMAL default null,
    PO2  DECIMAL default null,
    Segmented_Perc  DECIMAL default null,
    Neutrophil_Abs  DECIMAL default null,
    CO2  DECIMAL default null,
    Prothrombin_time  DECIMAL default null,
    Troponin_I  DECIMAL default null,
    TTP_Ratio  DECIMAL default null,
    Urea  DECIMAL default null,
    PRIMARY KEY (id_patient, date_analysis),
    FOREIGN KEY (id_patient) REFERENCES patient(id_patient)  ON DELETE CASCADE
    ) ENGINE = InnoDB;


CREATE TABLE allelePopulationData (
  id_var INT NOT NULL,
  id_group_disease INT NOT NULL,
  AC INT DEFAULT 0,
  AF FLOAT DEFAULT 0,
  AN INT DEFAULT 0,
  HomoC INT DEFAULT 0,
  AC_PseudoC INT DEFAULT 0,
  AF_PseudoC float DEFAULT 0,
  AN_PseudoC INT DEFAULT 0,
  HomoC_PseudoC INT DEFAULT 0
) ENGINE = InnoDB;

CREATE TABLE `alleleSubPopulationData` (
  `id_var` int NOT NULL,
  `short_name_diseases` varchar(100) NOT NULL,
  `AC` int DEFAULT '0',
  `AF` float DEFAULT '0',
  `AN` int DEFAULT '0',
  `HomoC` int DEFAULT '0',
  `AC_PseudoC` int DEFAULT '0',
  `AF_PseudoC` float DEFAULT '0',
  `AN_PseudoC` int DEFAULT '0',
  `HomoC_PseudoC` int DEFAULT '0'
) ENGINE=InnoDB;

#TABLES FIXES

INSERT INTO groupDisease ( name, short_name) values ('Distrofia de Retina','IRD');
INSERT INTO groupDisease ( name, short_name) values ('Distrofias Corneales', 'DisCor');
INSERT INTO groupDisease ( name, short_name) values ('Atrofia optica', 'AtrOpt');
INSERT INTO groupDisease ( name, short_name) values ('Neurodegeneracion','Neuro' );
INSERT INTO groupDisease ( name, short_name) values ('Metabolicas', 'Metab');
INSERT INTO groupDisease ( name, short_name) values ('Hemato Inmunologia','Hemat');
INSERT INTO groupDisease ( name, short_name) values ('Dermatologicas' , 'Derma');
INSERT INTO groupDisease ( name, short_name) values ('Encefalopatias, DI, Epilepsia','Encef');
INSERT INTO groupDisease ( name, short_name) values ('Enfermedad oftalmologica','Oftal');
INSERT INTO groupDisease ( name, short_name) values ('Malformaciones y Sind polimalformativos','Malform');
INSERT INTO groupDisease ( name, short_name) values ('Malformacion Ocular','MalfOc');
INSERT INTO groupDisease ( name, short_name) values ('Miopatias','Miop');
INSERT INTO groupDisease ( name, short_name) values ('Nefropatias','Nefro');
INSERT INTO groupDisease ( name, short_name) values ('Cancer','Cancer');
INSERT INTO groupDisease ( name, short_name) values ('Inflamatoria','Inflam');
INSERT INTO groupDisease ( name, short_name) values ( 'Cardiopatia','Cardio');
INSERT INTO groupDisease ( name, short_name) values ( 'Esterilidad','Ester');
INSERT INTO groupDisease ( name, short_name) values ( 'Digestivo','Diges');
INSERT INTO groupDisease ( name, short_name) values ( 'Hipoacusias','Hipoac');
INSERT INTO groupDisease ( name, short_name) values ( 'Prenatal','Prenatal');
INSERT INTO groupDisease ( name, short_name) values ( 'Endocrinologica','Endocrino');
INSERT INTO groupDisease ( name, short_name) values ( 'Varios','Varios');                     
INSERT INTO groupDisease ( name, short_name) values ('Neuropatias perifericas','Neurop');

insert INTO disease (id_groupe_disease, name, tag, inheritance) values (16, 'RIESGO CARDIOVASCULAR', 'CVD', 'AD, AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (16, 'CARDIOPATIA', 'CARDI', 'AD, AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (16, 'CARDIOGENETICA', 'CARDIOG', null);
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (7,'EPIDERMOLISIS BULLOSA', 'EB', 'AD, AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (1,'DISTROFIA DE CONOS', 'ACROM', 'AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (1,'DISTROFIA DE CONOS', 'ACRO', 'AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (1,'DISTROFIA DE CONOS', 'ACROMATOPSIA', 'AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (1,'DISTROFIA DE RETINA', 'RP', 'AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (1,'DISTROFIA MACULAR', 'MD', 'AD, AR, XLR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (1,'DISTROFIA MACULAR', 'MD(ARDM)', 'AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (1,'DISTROFIA MACULAR', 'MD(ADDM)', 'AD');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (1,'DISTROFIA MACULAR', 'MD(XLRS)', 'XLR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (1,'SINDROME DE NORRIE', 'NORRIE', 'XLR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (1,'RETINOSIS PIGMENTARIA', 'RRP', 'AD, XLR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (2,'DISTROFIA CORNEAL', 'DC', 'AD, AR, ');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (11,'ANIRIDIA', 'ANIRIDIA','AD');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (11,'ANIRIDIA', 'ANI','AD');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (11,'ANAOFTALMIA', 'ANF', 'AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (11,'ALBINISMO OCULAR', 'OALB', 'XLR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (11,'ALBINISMO OCULOCUTANEO', 'OCALB', 'AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (10,'ACONDROPLASIA', 'ACH', 'AD');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (10,'SINDROME DE MARFAN', 'MARFAN', 'AD');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (5,'FIBROSIS QUISTICA', 'FQ', 'AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (5,'HEMACROMATOSIS HEREDITARIA', 'HH', 'AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (12,'MIOPATIA', 'DM', 'AR, AD, XLR, XLD');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (12,'DISTROFIA MUSCULAR DUCHENNE/BECKER', 'DMD', 'XLR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (13,'FANCONI', 'NEFRO', 'AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (13,'VARIOS_NEFRO', 'NEFRO', 'AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (4,'APOE Y PRESENLINA', 'APOE', 'AD');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (4,'ATAXIA', 'ATAXIA', 'AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (4,'ATAXIA', 'ATAXIAS', 'AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (4,'ENFERMEDAD DE HUNTINGTON', 'CH', 'AD');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (4,'DEMENCIA FRONTO TEMPORAL', 'DFT', 'AD');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (4,'DEMENCIA FRONTO TEMPORAL', 'DTF', 'AD');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (4,'ENFERMEDAD DE KENNEDY', 'KENNEDY', 'XLR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (4,'ENFERMEDAD DE KENNEDY', 'KENEDY', 'XLR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (23,'CHARCOT-MARIE-TOOTH', 'CMT', 'AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (23,'DISTONIA', 'DYT', 'AD');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (23,'PARAPARESIA ESPASTICA FAMILIAR', 'PPE', null);
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (22, 'FAKE_DISEASE', 'VARIOS_FAKE', null);
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (3, 'ATROFIA OPTICA', 'OPA', 'AD, AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (14, 'CANCER', 'CA', null);
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (14, 'CANCER', 'CANCER', null);
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (8, 'SINDROME DE ANGELMAN', 'ANG', 'AD');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (8, 'SINDROME DE ANGELMAN', 'ANGELMAN', 'AD');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (8, 'SINDROME DE PRADER WILLY', 'PRADER', 'AD');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (8, 'SINDROME DE PRADER WILLY', 'PW', 'AD');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (8, 'SINDROME X FRAGIL', 'XFRA', 'XLR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (8, 'NO INFO', 'RM', null);
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (21, 'SINDROME DE KALMAN', 'KALLMAN', 'XLR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (17, 'VARIOS', 'EST', null);
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (17, 'VARIOS', 'ESTÉRIL', null);
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (17, 'VARIOS', 'ESTERIL', null);
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (19, 'VARIOS_SORDERA', 'SORDERA', null);
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (19, 'FIEBRE MEDITERRANEA FAMILIAR', 'FMF', 'AR');
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (20, 'PRENATAL', 'PRENATAL', null);
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (22, 'PPCI', 'PPCI', null);
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (22, 'VARIOS', 'VARIOS', null);
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (22, 'SINDROME HOLT HORAM', 'HH', null);
insert INTO disease (id_groupe_disease, name, tag, inheritance) values (22, 'SAMPLES_WO_FAM', 'FAKE', null);
