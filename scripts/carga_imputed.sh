path_ori="/mnt/genetica3/sheila/1806"
file_input="${path_ori}/imputed_filtered_new.vcf.gz"
file_mp="${path_ori}/mymetadatapathology_2021_06_07.txt"
file_sp="${path_ori}/samplePatient.csv"
file_conn="${path_ori}/scripts/dataConnexion.txt"
#the first is the imputed file compresed or uncompressed if the is an error about Number=R change it by Number=.

python ${path_ori}/scripts/carga_Imputed_Metadata.py \
"${file_input}" \
"${file_mp}" \
"${file_sp}" \
"${file_conn}" \
