#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 22:34:50 2021

@author: sheila
"""
#!/usr/bin/python
# -*- coding: utf-8 -*-

import pymysql as mdb
#import sys
import logging

class my_db():
    
    def __init__(self,  debug="ERROR", file_log_name="./db.log"):
        self.debug=debug
        self.logger = logging.getLogger(file_log_name)
        self.logger.setLevel( getattr(logging, debug) )

    def connect(self, host=None, user=None, passwd=None, database=None):
        '''Connect to the concrete data base. 
        The first time a valid host, user, passwd and database must be provided,
        Following calls can skip this parameters
        '''
        try:
            if host     is not None: self.host = host
            if user     is not None: self.user = user
            if passwd   is not None: self.passwd = passwd
            if database is not None: self.database = database         
            self.con = mdb.connect(host=self.host,
                             user=self.user,
                             password=self.passwd,
                             database=self.database,
                             cursorclass=mdb.cursors.DictCursor,
                             local_infile=1)
#            self.con = mdb.connect(self.host, self.user, self.passwd, self.database)
            self.logger.debug("connected to DB %s at %s@%s", self.database,self.user, self.host)
            return 0
        except mdb.Error as e:
            self.logger.error("Cannot connect to DB %s at %s@%s Error %d: %s", self.database, self.user, self.host, e.args[0], e.args[1])
            return -1 
    
    def get_connection(self):
        return self.con
    
    def disconnect(self):
        '''disconnect from the data base'''
        try:
            print("deconnexion")
            self.con.close()
            del self.con
        except mdb.Error as e:
            self.logger.error("while disconnecting from DB: Error %d: %s",e.args[0], e.args[1])
            return -1
        except AttributeError as e: #self.con not defined
            if e[0][-5:] == "'con'": return -1, "Database internal error, no connection."
            else: raise 



