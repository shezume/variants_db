path_ori="/mnt/genetica3/sheila/1806"
date_t=`date +"%d_%m_%Y_%H-%M"`
criter_patient="${path_ori}/criterios/patient_criteria.txt"
criter_disease="${path_ori}/criterios/disease_criteria.txt"
file_conn="${path_ori}/scripts/dataConnexion.txt"
file_export="${path_ori}/scripts/exportPopulationByCriteria_"$date_t".txt"


#values the first 2 paramenters are  liked to the patient and the disease, the 
#file connection is necessary to the database connexion
#the fourth parameter could have two values (DEFAULT OR DS)
#DEFAULT option calculate the pseudo control as ignoring the criteria selection (same as population calculation)
#DS calculate the pseudo control 
python ${path_ori}/scripts/calculateFrequencyCriteria.py \
"${criter_patient}" \
"${criter_disease}" \
"${file_conn}" \
DEFAULT \
"${file_export}" \


mysql -u root -pgenetica Variants_DB -e " select v.chrom, v.posStart, v.ref, v.alt,  p.AC, p.AF, p.AN, p.HomoC, p.AC_PseudoC, p.AF_PseudoC, p.AN_PseudoC, p.HomoC_PseudoC from alleleSubPopulationData p inner join variant v on (v.id_var=p.id_var)" >> "${file_export}" 
