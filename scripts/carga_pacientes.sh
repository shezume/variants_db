#!/bin/bash

path_ori="/mnt/genetica3/sheila/1806/scripts"

file_clin="${path_ori}/patients_files/clinical1_example.csv"
file_comorb="${path_ori}/patients_files/comorbidities_example.csv"
file_farma="${path_ori}/patients_files/drugs_example.csv"
file_conn="${path_ori}/scripts/dataConnexion.txt"

python2 ${path_ori}/scripts/cargaPacientesData.py \
"${file_clin}" \
"${file_comorb}" \
"${file_farma}" \
"${file_conn}" 
