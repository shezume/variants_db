#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 19 10:45:19 2021

@author: Sheila Zuniga Meza
"""

import logging
import pymysql
import os

#Dont put the keys in the table creation table it gets slowly all the treatment 
#add them before the insertion in the tables!!!!
"""--------------------------------------------------------------------------"""
"""                         CREATE DROPS TABLES                              """
"""--------------------------------------------------------------------------"""

def drop_table(connex, table_name):
    """ Erase the tmp table tmp_variant_AC
    Parameters
    ----------        
    conex: database connection
    """
    sql_drop="DROP TABLE IF EXISTS "+ table_name
    try:                 
        cursor=connex.cursor() 
        cursor.execute(sql_drop)
        cursor.close()        
    except pymysql.Error as e:          
        logging.error('problem in drop_'+ table_name)    
        logging.error(e)

def create_tmp_variant_AC(connex, table_name):
    """ Create table tmp_variant_AC
    Parameters
    ----------        
    conex: database connection
    """
    sql_create="create TABLE  " + table_name + "(id_var INT NOT NULL,\
                chrom VARCHAR(5) NOT NULL, posStart INT NOT NULL, ref TEXT, alt TEXT, AC int(11), HomoC int(11)) ENGINE = InnoDB"        
    try:                 
        cursor=connex.cursor() 
        cursor.execute(sql_create)

        cursor.close()        
    except pymysql.Error as e:          
        logging.error('problem in create_' + table_name)    
        logging.error(e)
         

def create_tmp_variant_AN(connex, table_name):
    """ Create table tmp_variant_AN
    Parameters
    ----------        
    conex: database connection
    """
    sql_create="create TABLE  " + table_name + "(\
                chrom VARCHAR(5) NOT NULL, posStart INT NOT NULL, AN int(11)) ENGINE = InnoDB"
    try:                 
        cursor = connex.cursor() 
        cursor.execute(sql_create)
        cursor.close()   
    except  pymysql.Error as e:           
        logging.error('Error in create_'+table_name, e)  


        
def create_variant_frequency(connex, table_name):
    """ Create table variant_frequency
    Parameters
    ----------        
    connex: database connection
    """
    logging.debug("create tmp:" + table_name)
    sql_create="create TABLE  " + table_name + "(\
                id_var int(11) not null, AF float, AC int(11), AN int(11), HomoC int(11)) ENGINE = InnoDB"
    try:                 
        cursor=connex.cursor()        
        cursor.execute(sql_create)
        cursor.close()
    except pymysql.Error as e:          
        logging.error('problem in create_' + table_name, e)   

def create_population_disease(connex):    
    """ create tmp table tmp_population_disease
    Parameters
    ----------        
    conex: database connection
    """
    sql_create="create TABLE  tmp_population_disease(id_var INT NOT NULL,\
        id_group_disease INT NOT NULL,\
        AC INT DEFAULT 0,\
        AF INT DEFAULT 0,\
        AN INT DEFAULT 0,\
        HomoC INT DEFAULT 0,\
        AC_PseudoC INT DEFAULT 0,\
        AF_PseudoC INT DEFAULT 0,\
        AN_PseudoC INT DEFAULT 0,\
        HomoC_PseudoC INT DEFAULT 0) ENGINE = InnoDB"
    try:                 
        cursor=connex.cursor()         
        cursor.execute(sql_create)
        cursor.close()
    except pymysql.Error as e:          
        logging.error('problem in create_population_disease')    
        logging.error(e)


"""--------------------------------------------------------------------------"""
"""                READ CONNEXION PARAMETERS                                 """
"""--------------------------------------------------------------------------"""

def read_connexion(input_file):
    """ Read the connexion parameters in the file passed as argument
    Parameters
    ----------        
    input_file: the file (name and path) containing the parameters separated by 
    \n such as (host, password, user, database)
    
    Return
    ------
    A dictionnaty containning the host, user, passzord and the database name
    """
    print("read_connexion")
    dic_connex = {}
    lst_terms = []
    if os.stat(input_file).st_size != 0:
        fd= open(input_file, 'r')
        for line in fd:            
            if(line[-1] == "\n"):
                lst_terms = line[:-1] .split("=")  
            else:
                lst_terms = line.split("=")            
                
            if(lst_terms[0].upper() == 'HOST' or lst_terms[0].upper() == 'USER' or\
                lst_terms[0].upper() == 'PASSWORD' or lst_terms[0].upper() == 'DATABASE'):
                if (lst_terms[0] not in dic_connex):
                    dic_connex[lst_terms[0]] = lst_terms[1]        
                        
        fd.close()
    return dic_connex
                

"""--------------------------------------------------------------------------"""
"""                         CALCULATE GENERAL FREQUENCYS                     """
"""--------------------------------------------------------------------------"""
         
def calculate_variant_frequency(connex):
    """ This fonction acts like the maint procedure to calculate the allele number,
    allele count and the homozygote allele count and the allele frequency of samples from 
    table tmp_sample for SNV variants. Those informations will be stored in variant_frequency_general
    
    Parameters
    ---------- 
    conex: database connection          
    """
    print("calculate variant frequency")    
    drop_table(connex, "variant_frequency_general")    
    create_variant_frequency(connex, "variant_frequency_general")    
    calculate_sample_general(connex)
    calculate_AN(connex, "tmp_variant_AN", "tmp_sample")
    calculate_AC(connex, "tmp_variant_AC", "tmp_sample")
    
    sql_insert = "insert into variant_frequency_general (id_var, AC, AN, AF, HomoC)\
        select vac.id_var, vac.AC, van.AN, round(AC/AN, 5), vac.HomoC\
        from tmp_variant_AC vac, tmp_variant_AN van\
        where vac.chrom=van.chrom and vac.posStart=van.posStart"        
    try: 
       cursor = connex.cursor() 
       cursor.execute(sql_insert)       
       cursor.close()
       connex.commit()
    except pymysql.Error as e:          
        logging.error('problem in calculate_variant_frequency', e)   

"""-------------------------------------------------------------------------"""
"""                       CALCULATE  FREQUENCYS COMMON                      """
"""-------------------------------------------------------------------------"""
    
def calculate_AN(connex, table_name, tmp_sample):
    """ Calculate the allele number for samples from the tmp_sample
    Parameters
    ---------- 
    conex: database connection          
    """
    print("calculate_AN de: " + table_name )
    drop_table(connex, table_name)
    create_tmp_variant_AN(connex, table_name)
    AN_value=0
    try:                 
        cursor = connex.cursor()   
        sql_count = "select count(*) as AN from "+  tmp_sample
        cursor.execute(sql_count)        
        res=cursor.fetchone()
        if res != None :
           AN_value=res['AN']
        
        sql_insert = "insert "+ table_name +"(chrom, posStart, AN) \
        Select v.chrom, v.posStart, 2*("+ str(AN_value) +"-sum(if(sv.genotype_count=-1, 1,0)) ) \
        from "+ tmp_sample +" s  \
        INNER JOIN sampleVariant sv on (s.id_sample=sv.id_sample)\
        INNER JOIN variant v on (v.id_var=sv.id_var)  \
        where  v.type='SNV'\
        group by v.chrom, v.posStart"
        #print(sql_insert)
        cursor.execute(sql_insert)        
        sql_create_primary = "ALTER TABLE " + table_name + \
                        " ADD CONSTRAINT PK_" + table_name + " PRIMARY KEY (chrom,posStart)"               
        cursor.execute(sql_create_primary)
        cursor.close()
        #print("fin insert")
    except  pymysql.Error as e:           
        logging.error('calculate_'+table_name, e)  
        
        

def calculate_AC(connex, table_name, tmp_sample):
    """ Calculate the allele count and the homozygote allele count HomoC
    for the samples from the tmo_sample
    
    Parameters
    ---------- 
    conex: database connection          
    """
    print("calculate_AC de: "+ table_name)
    drop_table(connex, table_name)
    create_tmp_variant_AC(connex, table_name)
    cursor = connex.cursor() 
    try:                 
        sql_insert = "insert into " + table_name + " (id_var, chrom, posStart, ref, alt, AC, HomoC)\
        Select v.id_var, v.chrom, v.posStart, v.ref, v.alt, sum(sv.genotype_count), sum(if(sv.genotype='HOM',sv.genotype_count, 0))/2 \
        from "+tmp_sample+" s  \
        INNER JOIN sampleVariant sv on (s.id_sample=sv.id_sample)\
        INNER JOIN variant v on (v.id_var=sv.id_var)  \
        where  sv.genotype_count<>-1 and v.type='SNV' \
        group by v.id_var"
        #print("insert:", sql_insert)
        cursor.execute(sql_insert) 
        sql_primary_key = "ALTER TABLE " + table_name +\
        " ADD CONSTRAINT PK_" + table_name + " PRIMARY KEY (id_var); "
        cursor.execute(sql_primary_key) 
                
        
        cursor.close()
    except pymysql.Error as e:          
         logging.error('problem in calculate_tmp_AC', e)   




def create_tmp_sample_gen(connex, table_name):
    """ Create a sample table containing one column with the name send as parameter
    
    Parameters
    ---------- 
    conex: database connection          
    """

    try:                 
        cursor = connex.cursor()       
        sql_create = "create TABLE  " + table_name + "(id_sample VARCHAR(7) primary key )"
        cursor.execute(sql_create)    
    except  pymysql.Error as e:          
        logging.error('create_tmp_sample' + table_name, e)  
        

  
def calculate_sample_general(connex):
    """ Get the tmp_samples for the general population treatment
    Parameters
    ---------- 
    conex: database connection      
    """    
    print("calculate_sample_general")
    drop_table(connex, "tmp_sample")
    create_tmp_sample_gen(connex, "tmp_sample")
    cursor=connex.cursor() 
    try:
        #every sample linked to the disease with independency of probandus
        sql_select= "insert into tmp_sample (id_sample) SELECT distinct(id_sample) FROM sample s \
        INNER JOIN patient p on (p.id_patient=s.id_patient)\
        INNER JOIN familyPatient fp on (p.id_patient =fp.id_patient) \
        INNER JOIN family f on (f.id_family=fp.id_family)\
        WHERE  fp.probandus=True"        
        cursor.execute(sql_select)
    except  pymysql.Error as e:          
        logging.error('calculate_sample_general', e)  

#elle sera la fonction generique pour les conditions    
def calculate_sample_condition(con, lst_disease, name_patient_table="patient", name_tmp_sample="tmp_sample"): 
    """ insert into sample table passed as parameter, the samples corresponding to the patient 
    table also passed as parameter and filtered by the diseases list
    
    Parameters
    ---------- 
    conex: database connection      
    lst_disease: the disease list as a filter pour this query
    name_tmp_sample: the tmp_saple as default this table will look for the tmp_sample table
    """  
    print("calculate_sample_condition")

    try:
        #every sample linked to the disease with independency of probandus
        sql_select= "insert into "+name_tmp_sample +"(id_sample) SELECT distinct(id_sample) FROM sample s \
        INNER JOIN "+ name_patient_table + " p on (p.id_patient=s.id_patient)\
        INNER JOIN familyPatient fp on (p.id_patient =fp.id_patient) \
        INNER JOIN family f on (f.id_family=fp.id_family)\
        INNER JOIN disease d on (d.tag = f.tag)\
        WHERE d.id_groupe_disease in ("+ lst_disease +") and fp.probandus=1"
        cursor=con.cursor() 
        cursor.execute(sql_select)       
        cursor.close()
    except  pymysql.Error as e:          
        logging.error('error in calculate_sample_condition', e)  
        
def calculate_sample_condition_gr22(connex):
    """ Get all the samples which are classified as various gr 22 and put them in the 
    tmp_sample_22 table
    
    Parameters
    ---------- 
    conex: database connection  
    """
    drop_table(connex, "tmp_sample_22")
    create_tmp_sample_gen(connex, "tmp_sample_22")
    cursor=connex.cursor() 
    print("calculate_sample_condition_gr22")
    try:
        #every sample linked to the disease with independency of probandus
        sql_select= "insert into tmp_sample_22 (id_sample) SELECT distinct(id_sample) FROM sample s \
        INNER JOIN patient p on (p.id_patient=s.id_patient)\
        INNER JOIN familyPatient fp on (p.id_patient =fp.id_patient) \
        INNER JOIN family f on (f.id_family=fp.id_family)\
        INNER JOIN disease d on (d.tag = f.tag)\
        WHERE d.id_groupe_disease=22 and fp.probandus=1"        
        cursor.execute(sql_select)
    except  pymysql.Error as e:          
        logging.error('error in calculate_sample_condition_gr22', e)  

def calculate_pseudo_control_condition(connex, name_tmp_sample= "tmp_sample", name_tmp_patient="patient"):
    """ insert the samples which are not contained in the tmp_sample neither in the varius samples 
    this samples will be used as pseudo control
    
    Parameters
    ---------- 
    connex: database connection  
    name_tmp_sample: the name of the table in which the samples are stored
    name_tmp_patient: the name of the patient table to be used
    """
    #precondition: tm_sample must be charged, dont need to know the condition
    #create table tmp de samples   
    print("calculate_pseudo_control_condition")
    cursor=connex.cursor() 
    try:
        #every sample used in pc
        sql_insert= "insert into tmp_sample_pc(id_sample) \
        SELECT distinct(s.id_sample) FROM sample s \
        INNER JOIN "+ name_tmp_patient +" p on (p.id_patient=s.id_patient)\
        INNER JOIN familyPatient fp on (p.id_patient =fp.id_patient)\
        INNER JOIN family f on (f.id_family=fp.id_family) \
        LEFT JOIN "+ name_tmp_sample +" tmp_s on (tmp_s.id_sample=s.id_sample)\
        LEFT JOIN tmp_sample_22 tmp_s2 on (tmp_s2.id_sample=s.id_sample)\
        WHERE tmp_s.id_sample is null and tmp_s2.id_sample is null and fp.probandus=1"    
        
        cursor.execute(sql_insert)
    except  pymysql.Error as e:           
        logging.error('calculate_pseudo_control_disease', e)  

    


def calculate_variant_frequency_disease(connex):
    """ sum up the temporary results of the AC an AN calculation and store then in the
    tmp_variant_frequency table"
    
    Parameters
    ---------- 
    conex: database connection  
    """

    print("calculate_variant_frequency_disease")
    
    drop_table(connex, "tmp_variant_frequency")    
    create_variant_frequency(connex, "tmp_variant_frequency" )   
    calculate_AN(connex, "tmp_variant_AN", "tmp_sample")
    calculate_AC(connex, "tmp_variant_AC", "tmp_sample")
    sql_index = "CREATE INDEX idx_chrom_pos_ac on tmp_variant_AC (chrom, posStart)"
    
    try:
        cursor = connex.cursor() 
        cursor.execute(sql_index)
        
        sql_insert = "insert into tmp_variant_frequency (id_var, AC, AN, AF, HomoC)\
            select vac.id_var, vac.AC, van.AN, if(AN=0, 0,round(AC/AN, 5)), vac.HomoC\
            from tmp_variant_AC vac, tmp_variant_AN van\
        where vac.chrom=van.chrom and vac.posStart=van.posStart"        
                    
        cursor.execute(sql_insert)   
        sql_primary_key = "ALTER TABLE tmp_variant_frequency\
        ADD CONSTRAINT PK_tmp_variant_frequency PRIMARY KEY (id_var)"
        cursor.execute(sql_primary_key) 
        connex.commit()
    except pymysql.Error as e:          
        logging.error('problem in tmp_variant_frequency', e)   

def calculate_pc_variant_frequency(connex):
    """ sum up the temporary results of the AC an AN calculation and store then in the
    tmp_variant_frequency table"    
    Parameters
    ---------- 
    conex: database connection  
    """
    print("calculate_pc_variant_frequency")
    cursor = connex.cursor() 
    drop_table(connex, "tmp_pc_variant_frequency")    
    create_variant_frequency(connex, "tmp_pc_variant_frequency" )     
    calculate_AN(connex,"tmp_variant_pc_AN", "tmp_sample_pc")
    calculate_AC(connex, "tmp_variant_pc_AC", "tmp_sample_pc")    
    sql_index = "CREATE INDEX idx_chrom_pos_ac_pc on tmp_variant_pc_AC (chrom, posStart)"
    
    try:
        cursor = connex.cursor() 
        cursor.execute(sql_index)
    
        sql_insert = "insert into tmp_pc_variant_frequency (id_var, AC, AN, AF, HomoC)\
        select vpac.id_var, vpac.AC, vpan.AN, if(AN=0, 0, round(AC/AN, 5)), vpac.HomoC\
        from tmp_variant_pc_AC vpac, tmp_variant_pc_AN vpan\
        where vpac.chrom=vpan.chrom and vpac.posStart=vpan.posStart"      
        
        #print(sql_insert)
        cursor.execute(sql_insert)      
        sql_primary_key = "ALTER TABLE tmp_pc_variant_frequency\
        ADD CONSTRAINT PK_tmp_pc_variant_frequency PRIMARY KEY (id_var) "
        cursor.execute(sql_primary_key) 
        connex.commit()
    except pymysql.Error as e:          
        logging.error('problem in calculate_pc_variant_frequency',e)   
        

def add_disease_population(connex, id_group_disease):
    """ the tmp_population_disease store the frequencys of the pseudocontrol and the disease    
    Parameters
    ---------- 
    conex: database connection  
    """
    #create table result
    print("Groupe disease", id_group_disease)
    cursor = connex.cursor() 
    sql_insert = "INSERT INTO tmp_population_disease (id_var,id_group_disease, AC, AF, AN, HomoC, \
    AC_PseudoC, AF_PseudoC, AN_PseudoC, HomoC_PseudoC) \
    SELECT tmp_var.id_var, %s, tmp_var.AC, tmp_var.AF, tmp_var.AN, tmp_var.HomoC, \
    tmp_pc.AC, tmp_pc.AF, tmp_pc.AN, tmp_pc.HomoC\
    FROM tmp_pc_variant_frequency tmp_pc\
    INNER JOIN tmp_variant_frequency tmp_var on tmp_var.id_var=tmp_pc.id_var"
    try:                 
        cursor.execute(sql_insert, [id_group_disease])
        
        connex.commit()
        return True
    except pymysql.Error as e:          
        logging.error('problem in add_disease_population')    
        logging.error(e)
    return False


