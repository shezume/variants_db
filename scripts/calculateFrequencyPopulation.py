#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 19 10:45:19 2021

@author: Sheila Zuniga Meza
"""

import logging
import pymysql
import calculation
import sys
import db
from datetime import datetime

"""--------------------------------------------------------------------------"""
"""                         CREATE TABLES                                     """
"""--------------------------------------------------------------------------"""
def create_allelePopulationData(connex):
    """ Empty the allelePopulationData used for the DS calculation
    Parameters
    ---------- 
    connex: database connection  
    """
    sql_create = "CREATE TABLE allelePopulationData (\
    id_var INT NOT NULL,\
    id_group_disease INT NOT NULL,\
    AC INT DEFAULT 0,\
    AF FLOAT DEFAULT 0,\
    AN INT DEFAULT 0,\
    HomoC INT DEFAULT 0,\
    AC_PseudoC INT DEFAULT 0,\
    AF_PseudoC float DEFAULT 0,\
    AN_PseudoC INT DEFAULT 0,\
    HomoC_PseudoC INT DEFAULT 0\
    ) ENGINE = InnoDB"
    try:                 
        cursor = connex.cursor() 
        cursor.execute(sql_create) 
        cursor.close()
    except pymysql.Error as e:          
        logging.error('problem in create_allelePopulationData')    
        logging.error(e)


"""-------------------------------------------------------------------------"""
"""                       CALCULATE  PAR DISEASE                            """
"""-------------------------------------------------------------------------"""
    
def  copy_tmp_def_population(connex):
    """ Copy in allelePopulationData the partial result contained in tmp_population_disease
    Parameters
    ---------- 
    conex: database connection   
    """
    sql_insert="INSERT INTO allelePopulationData (id_var,id_group_disease, AC, AF, AN, HomoC, \
    AC_PseudoC, AF_PseudoC, AN_PseudoC, HomoC_PseudoC) \
    SELECT id_var,id_group_disease, AC, AF, AN, HomoC, \
    AC_PseudoC, AF_PseudoC, AN_PseudoC, HomoC_PseudoC FROM tmp_population_disease"
    try:                 
        cursor=connex.cursor() 
        cursor.execute(sql_insert)
        connex.commit()
    except pymysql.Error as e:          
        logging.error('copy_tmp_def_population')    
        logging.error(e)
    
    
def calculate_population_disease(connex):
    """ This fonction calculate for each id_group disease the frequencys 
    for each disease and for its pseudocontrols
    Parameters
    ---------- 
    conex: database connection  
    """
    #empty de final table 
    calculation.drop_table(connex, "allelePopulationData")  
    create_allelePopulationData(connex)  
    sql_select= "select id_groupe_disease from groupDisease"
    try:                     
        cursor=connex.cursor()
        cursor.execute(sql_select)
        result = cursor.fetchall()
        #print(cursor._last_executed)
        calculation.calculate_sample_condition_gr22(connex)
        for disease_gr in result:            
            #empty the tmp table used to storage partial results
            calculation.drop_table(connex,"tmp_population_disease")
            calculation.create_population_disease(connex)
            #fill a tmp table with the samples which have the condition except Varios =22
            
            if(int(disease_gr['id_groupe_disease'])!=22 ):
                calculation.calculate_sample_condition_gr22(connex)
                calculation.drop_table(connex, "tmp_sample")
                calculation.create_tmp_sample_gen(connex, "tmp_sample")
                calculation.calculate_sample_condition(con=connex, lst_disease=str(disease_gr['id_groupe_disease']))
                calculation.drop_table(connex, "tmp_sample_pc")
                calculation.create_tmp_sample_gen(connex, "tmp_sample_pc")
                calculation.calculate_pseudo_control_condition(connex, "tmp_sample", name_tmp_patient="patient")        
                calculation.calculate_variant_frequency_disease(connex)
                calculation.calculate_pc_variant_frequency(connex)
                endT=False
                endT=calculation.add_disease_population(connex, disease_gr['id_groupe_disease'])
        
                if (endT):
                    copy_tmp_def_population(connex)
                    
        sql_primary_key="ALTER TABLE allelePopulationData\
        ADD CONSTRAINT PK_tallelePopulationData PRIMARY KEY (id_var, id_group_disease)"
        cursor.execute(sql_primary_key)
        cursor.close()   
        
    except pymysql.Error as e:          
        logging.error('problem in calculate_population_disease', e)  


def calculate_variant_frequency(connex):
    """ 
    Insert in variant_frequency_general the partial results of AC and AN
    Parameters
    ---------- 
    conex: database connection  
    """
    print("calculate variant frequency")    
    calculation.drop_table(connex, "variant_frequency_general")    
    calculation.create_variant_frequency(connex, "variant_frequency_general" )   
    calculation.calculate_sample_general(connex)
    calculation.calculate_AN(connex, "tmp_variant_AN", "tmp_sample")
    calculation.calculate_AC(connex, "tmp_variant_AC", "tmp_sample")
    
    sql_insert= "insert into variant_frequency_general (id_var, AC, AN, AF, HomoC)\
        select vac.id_var, vac.AC, van.AN, round(AC/AN, 5), vac.HomoC\
        from tmp_variant_AC vac, tmp_variant_AN van\
        where vac.chrom=van.chrom and vac.posStart=van.posStart"        
    #print(sql_insert)
    try: 
       cursor=connex.cursor() 
       cursor.execute(sql_insert)       
       cursor.close()
       connex.commit()
    except pymysql.Error as e:          
        logging.error('problem in tmp_variant_frequency', e)   
        
    
log_name='db_log_patient.log'
db_curr=db.my_db()

patientlogger = logging.getLogger(log_name)
patientlogger.setLevel( getattr(logging, "ERROR") )

patientlogger.debug('Main')


if len(sys.argv) == 2:        
    file_dataConn = sys.argv[1]    
    dic_param=calculation.read_connexion(file_dataConn)
    
    if len(dic_param)!=0:
        if (db_curr.connect(dic_param["host"], dic_param["user"],\
                            dic_param["password"], dic_param["database"])==0): #connex ok     
            conn=db_curr.get_connection()        
            print(datetime.now())
            #Add the total variant frequency
            calculate_variant_frequency(conn)
            #Add the total variant frequency
            calculate_population_disease(conn)
            print(datetime.now())
    else:
        print("Connexion Error")    

     
else:
    patientlogger.error('Error - Se espera el fichero de parametros de connexion como argumento')
    print("Error - Se espera el fichero de parametros de connexion como argumento")
    print('Ejemplo: calculateFrequencyPopulation.py "dataConnexion.txt"')

    
