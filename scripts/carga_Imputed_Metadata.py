#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  7 10:50:49 2021

@author: shei
"""
#https://pyvcf.readthedocs.io/en/latest/INTRO.html

import logging
import vcf
import pymysql
import time
import csv
import os
import db
import calculation
import sys
from datetime import date
from datetime import datetime
#import db
# attention verify that this parameter is set on the database SET GLOBAL local_infile=1;

dateT =date.today().strftime("%Y/%d/%m")


def truncate_tables(conex):
    """
    Empty the following tables sampleVariant, sample, variant linked to the sample 
    and variant 
    Parameters
    ----------
    conex: database connector        
    """
    cursor=conex.cursor()
    sqldeactivate ="SET FOREIGN_KEY_CHECKS = 0"; 
    cursor.execute(sqldeactivate)
    sql = "TRUNCATE TABLE sampleVariant"
    cursor.execute(sql)
    sql = "TRUNCATE TABLE sample"
    cursor.execute(sql)
    sql = "TRUNCATE TABLE variant"
    cursor.execute(sql)    
    sqlactivate ="SET FOREIGN_KEY_CHECKS = 1"; 
    cursor.execute(sqlactivate)
    sql_auto = "ALTER TABLE variant AUTO_INCREMENT = 1"
    cursor.execute(sql_auto)
    cursor.close()
    conex.commit()
    
def truncate_tablesFamily(conex):
    """
    Empty the following tables family, familyPatient linked to the family id
    Parameters
    ----------
    conex: database connector        
    """
    cursor=conex.cursor() 
    sqldeactivate ="SET FOREIGN_KEY_CHECKS = 0"; 
    cursor.execute(sqldeactivate)
    sql = "TRUNCATE TABLE family"
    cursor.execute(sql)
    sql = "TRUNCATE TABLE familyPatient"
    cursor.execute(sql)
    sqlactivate ="SET FOREIGN_KEY_CHECKS = 1"; 
    cursor.execute(sqlactivate)    

def upload_samples(lst_sample_name, conex):
    """ upload the sample list in the table sample, testing if the id_sample 
    exists in the database
    notice the id_project and the  id_patient are not upload in this step*    
    Parameters
    ----------
    lst_sample_name: iterable, containing the sample names 
    conex: database connection   
    """    
    print("upload_samples")
    
    for name in lst_sample_name:     
        sql = "SELECT id_sample FROM sample where id_sample=%s"
        try:
            cursor=conex.cursor()
            cursor.execute(sql,[name])            
            if(cursor.fetchone()==None):                    
                sql_insert = "insert into sample (id_sample) values (%s)"
                cursor.execute(sql_insert, [name])
            cursor.close()
        except pymysql.Error as e:
            logging.error('upload_samples', e)
            

def upload_variant_record_SNV(record, conex, outfile):
    """using a record of one specific variant the function will insert the variant
    in the database and compose a file which will containt the data to insert in 
    sample variant database
    
    Parameters
    ----------
    record: containing a line of the vcf reorganised by the pyvcf library
    conex: database connection
    outfile: the name of the file which containts the registers which will be 
    loaded in the sample variant table
    """    
    #print("upload_variant_record_SNV")
    sql_insert = "INSERT INTO variant ( chrom, posStart, posStop, ref, alt, type, info) values ( %s, %s,  %s, %s, %s, %s, %s)"
    try: 
        pos_allele_alt=0
        for alte in record.ALT:        
            #calculate end position
            posStop=record.POS
            if(len(record.REF)<len(alte)):
                posStop+=len(alte)-1
            pos_allele_alt+=1
            #print(record.INFO['AF'])
            #{'BaseQRankSum': 2.05, 'ExcessHet': 3.0103, 'FS': 0.0, 'MQ': 60.0, 'MQRankSum': 0.0, 'NDA': 2, 'QD': 13.13, 'ReadPosRankSum': 0.29, 'SOR': 0.609, 'DP': 191, 'AF': [0.5], 'MLEAC': [1], 'MLEAF': [0.5], 'AN': 8350, 'AC': [2]}            
            cursor=conex.cursor()   
            cursor.execute(sql_insert, [record.CHROM, record.POS, posStop, record.REF, str(alte), 'SNV', 'info'])
            cursor.close()
            id_var_next=get_id_variant_next(conex)   
   
            for sample in record.samples:
                #cursor2=conex.cursor()    
                genotype_record = sample['GT']                
                #print(genotype_record)
                genotype_count = 0
                genotype = 'REF'
                if (genotype_record != None):
                    if ("/" in genotype_record ):
                        alleles_list=genotype_record.split("/")
                    elif("|" in genotype_record ):
                        alleles_list=genotype_record.split("|")
                    else: 
                        raise ValueError("Genotype format don't have / or | symbol")               

                    if(alleles_list[0]==str(pos_allele_alt) or alleles_list[1]==str(pos_allele_alt)):
                        if((alleles_list[0]==alleles_list[1]) and alleles_list[0]!='0' and alleles_list[0]!='.' ):                                        
                            genotype_count=2
                            genotype='HOM'
                    
                        elif(alleles_list[0]==str(pos_allele_alt) or alleles_list[1]==str(pos_allele_alt) ):
                            if(alleles_list[0]=='0' or int(alleles_list[1])=='0'):
                                genotype='HET'
                            else:
                                genotype='HC'
                            genotype_count=1 #for HET and HC it will count 1 
                                
                else:
                    genotype_count= -1
                    genotype='NONE'  
                    
                if(genotype_count!=0): 
                    outfile.writerow([id_var_next-1, sample.sample, genotype, genotype_count, None ])
            
    except pymysql.Error as e:
         #conex.rollback()
         #conex.close()
        print(e)
        logging.error('erreur upload variant_record_SNV')


def  get_id_variant_next(conex):    
    """Getter of the next id_variant from variant table
    
    Parameters
    ----------    
    conex: database connection
    Return
    ------
    an id_variant or null if the query doesn't return a result
    """    
    try: 
        cursor=conex.cursor()
        sql_select = "SELECT max(id_var) as maximum from variant"
        cursor.execute(sql_select)        
        result = cursor.fetchone()             
        if(result != None):            
            cursor.close()
            return int(result['maximum'])+1            
        else:            
            return None
    except pymysql.Error as e:
        print(e)
        logging.error('get_id_variant')

def upload_variant_samples(input_file, conex, pipeline, outfile):    
    """ read the vcf file using the pyvcf package, insert the samples and 
        will try to insert the variants and the relation variantSample 
    
    Parameters
    ----------    
    input_file: the name of the vcf file
    conex: database connection
    pipeline: this parameter should have SNV or CNV, the last one is not imple-
    mented 
    outfile the name of the tmp file that will be dumped in the sampleVariant table
    """   
    logging.info('Update variant sample ')    
    print("upload_variant_samples")    
    fd = open(outfile, 'w+')
    out_writer = csv.writer(fd)
    cnt = 1
    if(pipeline == "SNV"):       
        vcf_reader = vcf.Reader(open(input_file, 'r'))
        lst_sample_name = []
        tmp_load=1
        for record in vcf_reader:
            #primera linea recupero los samplse para insertar en BD
            if tmp_load == 1:
                for sample in record.samples:
                    lst_sample_name.append(sample.sample)
                
                #update de BD if not exist sample 
                upload_samples(lst_sample_name, conex)
                conex.commit()
            #for all upload variants       
            upload_variant_record_SNV(record, conex, out_writer)
           
            if(tmp_load % 5500 == 0):
                print(cnt, " modulo5500")    
                cnt+=1
                fd.close()
                if(load_variants(conex, outfile)):                      
                    conex.commit()
                    fd = open(outfile, 'w+')
                    out_writer = csv.writer(fd)
            tmp_load+=1     
        fd.close()
        load_variants(conex, outfile) 
            
        #commit when the step is finish       
        conex.commit()

def load_variants(conex, outfile):
    """ read the vcf file using the pyvcf package, insert the samples and 
        will try to insert the variants and the relation variantSample 
    
    Parameters
    ----------        
    conex: database connection    
    outfile: the file to be loaded in the table 
    """
    if os.stat(outfile).st_size == 0:   
        return False
    else:        
        
        #sql_load= "insert into sampleVariant (id_var, id_sample) values (1, '00-0410')"
        sql_load = "load data local infile '"+outfile+"' into table sampleVariant FIELDS TERMINATED BY ',' LINES TERMINATED BY '\r\n' "        
        try:
            cursor=conex.cursor()
            cursor.execute(sql_load)           
            cursor.close()
            return True
        except pymysql.Error as e:
            print(e)
            logging.error('load_variants')
    
    
        
def get_projects (conex):
    """ getter of the projects ids
    
    Parameters
    ----------        
    conex: database connection    
    Result
    ------
    the query result containing the projects list     
    """
    sql_select = "SELECT id_project FROM project"    
    try:
        cursor=conex.cursor()
        cursor.execute(sql_select)
        result = cursor.fetchall()
        cursor.close()
        return result    
    except pymysql.Error as e:
        print(e)
        logging.error('get_projects')
        
def get_family (conex):
    """ getter of the family ids    
    Parameters
    ----------        
    conex: database connection    
    Result
    ------
    the query result containing the id family list     
    """
    
    sql_select = "SELECT id_family FROM family"    
    try:
        cursor=conex.cursor()
        cursor.execute(sql_select)
        result = cursor.fetchall()
        cursor.close()
        return result    
    except pymysql.Error as e:
        print(e)
        logging.error('get_family')
    
        
    
def update_projectSample (conex, project_name, id_sample):
    """ update the id project from the sample table if it was found in the project
    table
    
    Parameters
    ----------        
    conex: database connection  
    project_name: nom du projet 
    id_sample: sample id
        
    """     
    sql_select = "select id_project from project where id_project=%s"
    try:
        cursor = conex.cursor()                 
        cursor.execute(sql_select, [project_name])
        #there are no results
        if (cursor.rowcount == 1 ) :            
            sql_update = "update sample set id_project=%s where id_sample= %s "               
            cursor.execute(sql_update, [project_name, id_sample])
        cursor.close()
            
    except pymysql.Error as e:
        print(e)
        logging.error('update_projectSample_query_select') 


def update_project(conex, project_name):
    """ Insert a project if it doesnt exists
    
    Parameters
    ----------        
    conex: database connection  
    project_name: nom du projet     
    """     
    #recuperer tous les projets inseres
    result = get_projects(conex)    
    not_found=True         
    for project in result:      
        if(project_name == project['id_project']):
                not_found = False 
                break
    if(not_found):                
        cursor=conex.cursor()    
        sql_insert = "insert into project (id_project, type_project) values (%s, %s) ON DUPLICATE KEY UPDATE id_project=id_project;"
        type_project=project_name[0:3]              
        if (type_project.upper()=='PAN' or type_project.upper()=='IDE'):
            type_project='TSO'
        try:                 
            cursor.execute(sql_insert, [project_name,  type_project])
            cursor.close()
        except pymysql.Error as e:
            print(e)
            logging.error('update_project')

def update_family(conex, family_name, tag, cat):
    """ insert into the family table the family and the disease tag
    if the family name is not fill it will be associated to fake family
    Parameters
    ----------        
    conex: database connection  
    family_name: nom du projet  
    tag: the abreviation of the disease 
    cat: contains the group_disease but it could be NA, that we change by VARIOS
    """    
    #get all familys inserted
    #print("updatefamily", family_name, tag)
    if(family_name!="-"):            
        sql_insert = "insert into family (id_family, tag) values (%s, %s ) ON DUPLICATE KEY UPDATE tag=tag"                
        try:                 
            cursor = conex.cursor()              
            if (tag=='(ca' or tag=='(CA'):
                print("varios ", cat, " tttt")
                
            if((cat[:2] == "NA" and len(cat) <= 3) or cat == 'VARIOS'):                
                cursor.execute(sql_insert, [family_name,  'VARIOS'])                
            else:
                cursor.execute(sql_insert, [family_name,  tag])                                
            cursor.close()
        except pymysql.Error as e:            
            logging.error('update_family',e)
    else:
        sql_fake_fam="insert into family (id_family, tag) values ('FAKE_FAMILY', 'FAKE') ON DUPLICATE KEY UPDATE tag=tag"
        try:                 
            cursor = conex.cursor()    
            cursor.execute(sql_fake_fam)                
            cursor.close()
        except pymysql.Error as e:
            print(e)
            logging.error('update_family -fake')
   
def find_patient(connex, id_sample):    
    """ find a patient using his id_sample
    Parameters
    ----------        
    conex: database connection  
    id_sample: the id sample linked to the patient
    """    
    
    sql_select="select distinct(id_patient) as id_patient from sample where id_sample=%s"
    
    cursor=connex.cursor() 
    try:           
        cursor.execute(sql_select, [id_sample])    
        result = cursor.fetchone()
        cursor.close()
        if result != None :
            return result['id_patient']
        else:
            return None
    except pymysql.Error as e:
        logging.error('find_patient', e)
    
   
def upload_familyPatient_file(connex, family, sample, outfile):
    """ insert the relation family patient in outfile
    Parameters
    ----------        
    conex: database connection  
    family: family id   
    sample: the id sample linked to the patient 
    outfile: the location of the file used as a temporal to write this information
    """    
    
    id_patient=find_patient(connex, sample)  
    if(id_patient != None):        
        try:             
            outfile.writerow( [family, id_patient, 1])           
        except pymysql.Error as e:
            print(e)
            logging.error('upload_familyPatient'+family+' '+sample)
    else:
        logging.debug("upload_familyPatient sample skip ",   sample)

def load_familyPatient(conex, fam_file_out):
    """ 
    insert the file in parameter fam_file_out in familyPatient table
    Parameters
    ----------        
    conex: database connection  
    fam_file_out: the location/file whith the information (family, patient, probandus)
    """  
    print("load_familyPatient", fam_file_out)
    if os.stat(fam_file_out).st_size == 0:
        return False
    else:                
        sql_load = "load data local infile %s into table familyPatient FIELDS TERMINATED BY ','"       
        try:
            cursor = conex.cursor()
            cursor.execute(sql_load, [fam_file_out])
            #print(cursor._last_executed)
            cursor.close()
            return True
        except pymysql.Error as e:
            print(e)
            logging.error('load_familyPatient')
    
def upload_family_project(conex, input_file, fam_file_out):
    """ 
    Read the metadatapathology file and fill in the database the family, project and 
    relations between those fields
    
    Parameters
    ----------        
    conex: database connection  
    input_file: the path of metadatapathology file
    fam_file_out: the location/file to store temporary the (family, patient, probandus)
    """
    fd= open(input_file, 'r')
    line=fd.readline()
    field_lst=line.split('\t')        
    i=0
    pos_project=2
    pos_family=1
    pos_tag=4
    pos_sample=3
    pos_cat=6
    for field in field_lst:
        if(field.upper()=='FAMILIA'):
            pos_family=i
        if(field.upper()=='PROYECTO'):
            pos_project=i
        if(field.upper()=='SAMPLE'):
            pos_sample=i
        if(field=='TAG'):
            pos_tag=i
        if(field.upper()=='CATEGORIA'):
            pos_cat=i
        i+=1
    fd2=open(fam_file_out, 'w+')
    out_writer = csv.writer(fd2)
    truncate_tablesFamily(conex)
   
    for line in fd:
        field_lst=line.split('\t')     
        
        #project in the second column of mymetadatapathology     
        update_project(conex, field_lst[pos_project].upper().rstrip('\n'))                
        update_family(conex, field_lst[pos_family].upper()[0:40], field_lst[pos_tag].upper().rstrip('\n'), field_lst[pos_cat].upper().rstrip('\n') )
    
        if(field_lst[pos_family].upper()!="-"): 
            upload_familyPatient_file(conex, field_lst[pos_family].upper()[0:40],field_lst[pos_sample].upper(), out_writer)
        else:
            upload_familyPatient_file(conex, 'FAKE_FAMILY', field_lst[pos_sample].upper(), out_writer)        
        update_projectSample(conex, field_lst[pos_project].upper(), field_lst[pos_sample].upper())      
    fd2.close()    
    load_familyPatient(conex, fam_file_out)
        
    conex.commit()            
    fd.close()  

def sample_patient(conex, input_file):
    """ 
    Fill the relation between the patient and the sample 
    
    Parameters
    ----------        
    conex: database connection  
    input_file: the path of metadatapathology file  
    """
    fd= open(input_file, 'r')
    line=fd.readline()
    for line in fd:        
        
        field_lst=line[:-1].split(',')          
        sql_update="UPDATE sample SET id_patient=%s WHERE id_sample=%s"
        try:           
            cursor=conex.cursor() 
            cursor.execute(sql_update, [field_lst[1],  field_lst[0]])    
            #print(cursor._last_executed)            
            cursor.close()
        except pymysql.Error as e:
            print(e)
            logging.error('update_project')    
    conex.commit()            
    fd.close()  


    
tmp_out = "./tmp_variants_sample3.csv"
tmp_out_familyPatient = "./famPatient.csv"
#MANQUE recuperer les parametres de ligne de comandes 
db_curr=db.my_db()

print("MAIN")


if len(sys.argv) == 5:        
    file_dataConn = sys.argv[4]    
    dic_param=calculation.read_connexion(file_dataConn)    
    
    if len(dic_param)!=0:
        if (db_curr.connect(dic_param["host"], dic_param["user"],\
                            dic_param["password"], dic_param["database"])==0): #connex ok     
            conn=db_curr.get_connection()     
            
            #empty tables 
            print(datetime.now())
            truncate_tables(conn)
    
            # update variants and samples 
            if (not os.path.isfile(tmp_out)):
                open(tmp_out, 'a').close()
            input_file = sys.argv[1]   
          
            upload_variant_samples(input_file, conn, 'SNV', tmp_out)         
   
            print("sample begin")
            print(datetime.now())
            file_sp = sys.argv[3]   
            sample_patient(conn, file_sp)
            #Metadatapathology 
            print("family_project")
            print(datetime.now())
            if (not os.path.isfile(tmp_out_familyPatient)):               
                open(tmp_out_familyPatient, 'a').close()
            
            file_mp = sys.argv[2]   
            upload_family_project(conn, file_mp, tmp_out_familyPatient)

            conn.commit()
            print(datetime.now())
            conn.close()
    
            
    else:
        print("Connexion Error")    


    
