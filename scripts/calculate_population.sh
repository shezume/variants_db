path_ori="/mnt/genetica3/sheila/1806"
date_t=`date +"%d_%m_%Y_%H-%M"`
file_conn="${path_ori}/scripts/dataConnexion.txt"
file_export="${path_ori}/scripts/exportPopulation_"$date_t".txt"

python ${path_ori}/scripts/calculateFrequencyPopulation.py \
"${file_conn}" \

mysql -u root -pgenetica Variants_DB -e "select gd.short_name, v.chrom, v.posStart, v.ref, v.alt, p.AC, p.AF, p.AN, p.HomoC, p.AC_PseudoC, p.AF_PseudoC, p.AN_PseudoC, p.HomoC_PseudoC from allelePopulationData p inner join variant v on (v.id_var=p.id_var) inner join groupDisease gd on (gd.id_groupe_disease=p.id_group_disease) order by gd.short_name" >> "${file_export}" 
