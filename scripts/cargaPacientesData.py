#!/usr/bin/env python2

# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 10:59:48 2021

@author: sheila
"""
import db
import pymysql
import logging
import sys
import calculation

def truncate_table_patient(conex):    
    """ Empty patient table 
    Parameters
    ----------        
    conex: database connection      
    """    
    cursor = conex.cursor() 
    sqldeactivate  = "SET FOREIGN_KEY_CHECKS = 0"; 
    cursor.execute(sqldeactivate)
    sql = "TRUNCATE TABLE patient"
    cursor.execute(sql)
    sqlactivate = "SET FOREIGN_KEY_CHECKS = 1"; 
    cursor.execute(sqlactivate)
    cursor.close()
    
def truncate_table_comorbility(conex):    
    """ Empty patientComorbility table  
    Parameters
    ----------        
    conex: database connection      
    """ 
    cursor = conex.cursor() 
    sqldeactivate = "SET FOREIGN_KEY_CHECKS = 0"; 
    cursor.execute(sqldeactivate)
    sql = "TRUNCATE TABLE patientComorbility"
    cursor.execute(sql)
    sqlactivate = "SET FOREIGN_KEY_CHECKS = 1"; 
    cursor.execute(sqlactivate)
    cursor.close()
    
def truncate_table_drugs_patient(conex):   
    """ Empty the following tables patientDrugs and drugs
    Parameters
    ----------        
    conex: database connection
    """
    cursor = conex.cursor() 
    sqldeactivate ="SET FOREIGN_KEY_CHECKS = 0"; 
    cursor.execute(sqldeactivate)
    sql = "TRUNCATE TABLE drugs"
    cursor.execute(sql)
    sql = "TRUNCATE TABLE patientDrugs"
    cursor.execute(sql)
    sqlactivate = "SET FOREIGN_KEY_CHECKS = 1"; 
    cursor.execute(sqlactivate)
    cursor.close()
    

def patient_clin(conex, input_file):
    """ insert into table patient data(age, gender, weight, height) found in 
    a input_file
    Parameters
    ----------        
    conex: database connection
    input_file: a file in table format containg at least the following columns 
    (age, gender, weight, height)
    """
    fd = open(input_file, 'r')
    line = fd.readline()
    field_lst = line.split(',')  
    i = 0
    pos_gender = 7
    pos_age = 4
    pos_weight = 5
    pos_height = 6
    pos_id = 0
    for field in field_lst:
        if(field.upper() == 'AGE'):
            pos_age=i
        if(field.upper() == 'GENDER'):
            pos_gender=i
        if(field.upper() == 'WEIGHT'):
            pos_weight=i
        if(field.upper() == 'HEIGHT'):
            pos_height=i
        if(field.upper() == 'ID_PATIENT'):
            pos_id=i    
        i+=1
    
    try:
        cursor=conex.cursor() 
        truncate_table_patient(conex)
    except pymysql.Error as e:
        print(e)
        logging.error('patient _drop')
        
    for line in fd:
        field_lst=line.split(',')      
        
        sql_insert="insert into patient (id_patient, age, weight, height, gender) \
        values (%s, %s, %s, %s,%s)\
        ON DUPLICATE KEY UPDATE age=%s, weight=%s, height=%s, gender=%s"
        
        if (field_lst[pos_gender] == '0'):
            gender = 'M'
        else:
            gender = 'F'
        if(field_lst[pos_age] == 'NA'):
            age = None
        else:
            age = field_lst[pos_age]        
        if(field_lst[pos_weight] == 'NA'):
            weight = None
        else:
            weight = field_lst[pos_weight]
        if(field_lst[pos_height] == 'NA'):
            height = None
        else:
            height = field_lst[pos_height]
            
        try:               
            cursor.execute(sql_insert, [field_lst[pos_id], age,  weight, height, gender,\
                                        age, weight, height, gender])                
        except pymysql.Error as e:
            print(e)
            logging.error('patient')
    cursor.close()
    conex.commit()            
    fd.close()  

def comorbility(conex, input_file):
    """ upload  into table patientComorbility data(id patient, and the comorbility columns)
    (FUMADOR, CARDIO, PULMONAR, DIABETES, RENAL, ONCO, HTA) all relation existent or not will 
    be upload
    
    Parameters
    ----------        
    conex: database connection
    input_file: a file in table format containg at least the following columns 
    (FUMADOR, CARDIO, PULMONAR, DIABETES, RENAL, ONCO, HTA)
    """
    fd = open(input_file, 'r')
    line = fd.readline()
    field_lst = line[:-1].split(',')  
    i = 0
    pos_fumador = 0
    pos_cardi = 0
    pos_diabetes = 0
    pos_renal = 6
    pos_neuro = 0
    pos_onco = 0
    pos_hta = 0
    pos_pulmonar = 0
    pos_id = 0
    for field in field_lst:
        print(field.upper())
        if(field.upper() == 'FUMADOR'):
            pos_fumador=i
        if(field.upper() == 'CARDIO'):
            pos_cardi=i
        if(field.upper() == 'PULMONAR'):
            pos_diabetes=i
        if(field.upper() == 'DIABETES'):
            pos_pulmonar=i
        if(field.upper() == 'RENAL'):
            pos_renal=i     
        if(field.upper() == 'NEURO'):
            pos_neuro=i     
        if(field.upper() == 'ONCO'):
            pos_onco=i    
        if(field.upper() == 'HTA'):
            pos_hta=i              
        i+=1

    try:
        cursor = conex.cursor() 
        truncate_table_comorbility(conex)
    except pymysql.Error as e:
        print(e)
        logging.error('patient comorbilite truncate')
        
    for line in fd:
        field_lst = line[:-1].split(',')      
     
        if(field_lst[pos_fumador] == 'NA' or field_lst[pos_fumador] == ''):
            smocker = None
        else:
            smocker = field_lst[pos_fumador]
        if(field_lst[pos_cardi] == 'NA'  or field_lst[pos_cardi] == ''):
            cardi = None
        else:
            cardi = field_lst[pos_cardi]
            
        if(field_lst[pos_diabetes] == 'NA' or field_lst[pos_diabetes] == ''):
            diabetes = None
        else:
            diabetes=field_lst[pos_diabetes]
        if(field_lst[pos_renal] == 'NA' or field_lst[pos_renal] == ''):
            renal = None
        else:
            renal=field_lst[pos_renal]
        if(field_lst[pos_neuro]=='NA' or field_lst[pos_neuro]==''):
            neuro = None
        else:
            neuro = field_lst[pos_neuro]
        if(field_lst[pos_onco] == 'NA' or field_lst[pos_onco] == ''):
            onco = None
        else:
            onco = field_lst[pos_onco]
      
        if(field_lst[pos_id] == 'NA'):
            id_patient = None
        else:
            id_patient = field_lst[pos_id]
        if(field_lst[pos_pulmonar] == 'NA' or field_lst[pos_pulmonar] == ''):
            pulmonar = None
        else:
            pulmonar = field_lst[pos_pulmonar]
        if(field_lst[pos_hta] == 'NA' or field_lst[pos_hta] == ''):
            hta = None
        else:
            hta = field_lst[pos_hta]
            
        
         
        sql_insert = "insert into patientComorbility(id_patient, type_comorb, value_comorb) \
        values \
        (%s, 'SMOKER', %s),\
        (%s, 'CARDIO', %s),\
        (%s, 'DIABETES', %s),\
        (%s, 'RENAL', %s),\
        (%s, 'NEURO', %s),\
        (%s, 'ONCO', %s),\
        (%s, 'HTA', %s),\
        (%s, 'PULMONARY', %s)"
        try:               
            cursor.execute(sql_insert, [id_patient, smocker, id_patient, cardi,\
                                        id_patient, diabetes, id_patient, renal,\
                                        id_patient, neuro, id_patient, onco,\
                                        id_patient, hta, id_patient, pulmonar])        
            
           
        except pymysql.Error as e:
            print(e)
            logging.error('comorbilites')
    cursor.close()
    conex.commit()            
    fd.close()  


def drugs_patient(conex, input_file):
    """ upload  into table drugs all the drugs found in the header
    using that information compose the relations patiendDrugs only when the relation
    exists (value=1)
    
    Parameters
    ----------        
    conex: database connection
    input_file: a file in table format containg the id_patient and the name of the drugs in the header 
    starting in the the fourth column
    """
    fd = open(input_file, 'r')
    line = fd.readline()
    field_lst = line.split(',')  
 
    lst_drugs = field_lst[4:]        
    
    try:
        cursor = conex.cursor() 
        truncate_table_drugs_patient(conex)
        sql_insert_drug = "insert into drugs (drug_name) values "
        for drug in lst_drugs:
            sql_insert_drug = sql_insert_drug + "('"+drug+"'),"
        sql_insert_drug = sql_insert_drug[:-1]        
        
        cursor.execute(sql_insert_drug)
        cursor.close()
        
        for line in fd:
            field_lst = line.split(',')  
            lst_drugs = field_lst[4:]

            sql_insert_patient_drug = "insert into patientDrugs (id_patient, id_drug) values "
            i=1
            for drug_value in lst_drugs:
                id_drug = i 
                if(drug_value == '1'):
                    sql_insert_patient_drug = sql_insert_patient_drug + \
                    "('" + field_lst[0] + "', '" + str(id_drug) + "' ),"
                i+=1
           
            if(sql_insert_patient_drug[-1] == ',' ):
                sql_insert_patient_drug = sql_insert_patient_drug[:-1]   
                #print(sql_insert_patient_drug)
                cursor = conex.cursor() 
                cursor.execute(sql_insert_patient_drug)
                cursor.close()
        conex.commit()  
    except pymysql.Error as e:
        print(e)
        logging.error('patient comorbilite truncate')
        
    cursor.close()
    conex.commit()            
    fd.close()  




log_name = 'db_log_patient.log'
#db_curr=db.my_db(file_log_name=log_name)
host = 'localhost'
user = 'sheila'
password = 'password' 
database = 'Variants_DB'
#file_p = "clinical1_example.csv"

patientlogger = logging.getLogger('patient_upload.log')
patientlogger.setLevel( getattr(logging, "ERROR") )

patientlogger.debug('Main')
db_curr = db.my_db()
if len(sys.argv) == 5:    
    file_clin = sys.argv[1]
    file_comorb = sys.argv[2]
    file_farma = sys.argv[3]
    file_dataConn = sys.argv[4]   
    dic_param = calculation.read_connexion(file_dataConn)
    
    if len(dic_param) != 0:
        if (db_curr.connect(dic_param["host"], dic_param["user"],\
                            dic_param["password"], dic_param["database"]) == 0): #connex ok     
            con = db_curr.get_connection()
            patient_clin(con, file_clin)
            comorbility(con, file_comorb)
            drugs_patient(con, file_farma)
    else:
        print("connex echec")    

else:
    patientlogger.error('Error - Se esperan 3 ficheros para realizar la carga')
    print("Error - Se esperan 3 ficheros para realizar la carga")
    print('Ejemplo: CargaPacientsData.py "clin.csv" "comorb.csv" "farma.csv"')


