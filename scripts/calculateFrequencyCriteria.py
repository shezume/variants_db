#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 23:04:36 2021

@author: Sheila Zuniga Meza
"""
import db
import pymysql
import logging
import sys
import re
import os
import calculation
from datetime import datetime
#SET GLOBAL innodb_buffer_pool_size=42865786880;
                                       
def create_alleleSubPopulationData(connex):
    """ Empty the allelePopulationData used for the DS calculation
    Parameters
    ---------- 
    connex: database connection  
    """
    sql_create = "CREATE TABLE `alleleSubPopulationData` (\
                `id_var` int NOT NULL,\
                `short_name_diseases` varchar(100) NOT NULL,\
                `AC` int DEFAULT '0',\
                `AF` float DEFAULT '0',\
                `AN` int DEFAULT '0',\
                `HomoC` int DEFAULT '0',\
                `AC_PseudoC` int DEFAULT '0',\
                `AF_PseudoC` float DEFAULT '0',\
                `AN_PseudoC` int DEFAULT '0',\
                `HomoC_PseudoC` int DEFAULT '0'\
                )ENGINE=InnoDB;"
    try:                 
        cursor = connex.cursor() 
        cursor.execute(sql_create) 
        cursor.close()
    except pymysql.Error as e:          
        logging.error('problem in alleleSubPopulationData')    
        logging.error(e)

def create_tmp_patient(connex, tablename):
    """ Create a table with one column the id_patient named as the parameter 
    tablename
    
    Parameters
    ---------- 
    connex: database connection 
    tablename: the table name that will be created
    """    
    sql_create = "create TABLE  " + tablename + " (id_patient INT primary key\
                ) ENGINE = InnoDB"        
    try:                 
        cursor=connex.cursor() 
        cursor.execute(sql_create)        
        cursor.close()      
        
    except pymysql.Error as e:          
        logging.error('problem in create_tmp_variant_AC')    
        logging.error(e)

def drop_population_criteria(connex):
    """ Destroy tmp table drop_population_criteria
    """
    sql_drop = "DROP TABLE IF EXISTS tmp_population_criteria"
    try:                 
        cursor = connex.cursor() 
        cursor.execute(sql_drop)   
        cursor.close()
    except pymysql.Error as e:          
        logging.error('problem in drop_population_criteria')    
        logging.error(e)
         

def create_population_criteria(connex):    
    """ create tmp table tmp_population_criteria
    """
    sql_create = "create TABLE  tmp_population_criteria(id_var INT NOT NULL,\
        AC INT DEFAULT 0,\
        AF FLOAT DEFAULT 0,\
        AN INT DEFAULT 0,\
        HomoC INT DEFAULT 0,\
        AC_PseudoC INT DEFAULT 0,\
        AF_PseudoC float DEFAULT 0,\
        AN_PseudoC INT DEFAULT 0,\
        HomoC_PseudoC INT DEFAULT 0) ENGINE = InnoDB"
    try:                 
        cursor = connex.cursor()         
        cursor.execute(sql_create)
        cursor.close()
    except pymysql.Error as e:          
        logging.error('problem in create_population_disease')    
        logging.error(e)
#-------------------------------------------------------------------------
def lst_disease_to_txt (lst_gr_disease):
    """ convert the lst_gr_disease into a string separated by colon
    Parameters
    ----------    
    lst_gr_disease: the list to be transformed
    
    Return
    ------
    string containing the values of the list separated by colon 
    """
    txt_gr_disease = ""
    for gr_disease in lst_gr_disease:
        txt_gr_disease+="'"+str(gr_disease)+"',"   
    if(txt_gr_disease[-1] == ","):    
        txt_gr_disease = txt_gr_disease[:-1]
    return txt_gr_disease
        
def convert_file_list(connex, file_in):
    """ convert the data received in the file into a list
    Parameters
    ---------- 
    connex: database connection 
    file_in: the input file
    
    Return
    ------
    a list of elements read from the input file
    """
    fd = open(file_in, 'r')       
    lst_elts = ""
    for line in fd:
        field_lst = line.split('\t')  
        lst_elts = lst_elts + "'" + field_lst[0] + "', "
    
    if(lst_elts[-1] == ',' ):
        lst_elts = lst_elts[:-1]   
    fd.close()
    return lst_elts
    
def get_criteria_disease(connex, file_disease, dic_param):    
    """ Read the disease criteria and obtaint the id_groupe_diseases then 
    store it in the dictionary dic_param
    Parameters
    ---------- 
    connex: database connection 
    file_disease: the file containing the disease criteria
    dic_param: the dictionary containing the request parameteres
    
    Return
    ------
    list of id from the group_disease
    """
    if os.stat(file_disease).st_size == 0:
        return []
    else: 
        fd = open(file_disease, 'r')       
        lst_ids = []
        lst_diseases = []
        sql_groupDisease = "select id_groupe_disease from groupDisease where short_name =%s"
        cursor=connex.cursor()
        for line in fd:
            field_lst = line.split('\n')
            try:
                cursor.execute(sql_groupDisease, [field_lst[0].replace(" ","")])
                #print(cursor._last_executed)
                res=cursor.fetchone()
                if res != None :
                    lst_ids.append(res['id_groupe_disease'])   
                    lst_diseases.append(field_lst[0].replace(" ",""))
                else:
                    print("WARNING CHECK: " + field_lst[0] + "doesn' found in groupDisease table")
            except pymysql.Error as e:          
                logging.error('problem in calculate_population_disease', e)    
            
        fd.close()
        cursor.close()
        dic_param["diseases"] = lst_diseases
        return lst_ids


def get_criteria_patient(connex, file_patient):
    """ Read the patient criteria contained in a file named as the "file_patient" in parameter
    with this criteria the function build a table tmp_patient which is a subset of the patient table
    filtered by this criteria
    
    Parameters
    ---------- 
    connex: database connection 
    file_patient: the file containing the patient criteria  
    
    Return
    ------
    a dictionary containing the criteria passed 
    """
    
    fd = open(file_patient, 'r')        
    string_comorb = ""
    string_drugs = ""
    string_age = ""
    string_gender = ""  
    string_weight = ""
    calculation.drop_table(connex, "tmp_patient")
    create_tmp_patient(connex,"tmp_patient")
    connex.commit()
    dic_param = {}
    
    for line in fd:        
        field_lst = re.split(',|:', line[:-1])  
        if (field_lst[0].upper() == 'AGE'):
            if(len(field_lst) == 2 and field_lst[1].replace(" ","")!=""):
                string_age = " p.age=" + field_lst[1].replace(" ","")
               
            elif (len(field_lst) == 3):
                string_age = " p.age between " + field_lst[1].replace(" ","")  + " and " +\
                field_lst[2].replace(" ","") 
            
            dic_param['AGE']= string_age
                                
        if (field_lst[0].upper() == 'WEIGHT'):
            
            if(len(field_lst) == 2 and field_lst[1].replace(" ","")!=""):
                string_weight = " p.weight=" + field_lst[1].replace(" ","")
               
            elif (len(field_lst) == 3):
                string_weight = " p.weight between " + field_lst[1].replace(" ","")  + " and " +\
                field_lst[2].replace(" ","")   
            dic_param['WEIGHT']= string_weight    
                
        if (field_lst[0].upper() == 'GENDER' and len(field_lst) == 2):
            if (field_lst[1].replace(" ","") in ('F','M')):
                string_gender = " p.gender='"+field_lst[1].replace(" ","")+"'"
                dic_param['GENDER']= string_gender   
            else:
                print("WARNING: Gender value not valid please select F or M or nothing to ignore this criteria")
            
        if (field_lst[0].upper() == 'COMORBILITY'):
            if( len(field_lst) >= 2):
                string_comorb = " pc.type_comorb in ("
                string_not_comorb = " pc.type_comorb in ("
                nb_comorb=0
                nb_not_comorb=0
                cursorComorb=connex.cursor()
                sql_enum_comorb="SELECT REPLACE(SUBSTRING_INDEX(SUBSTRING(column_type,6),')',1),'\\'','')\
                 as enume FROM information_schema.COLUMNS WHERE table_schema='Variants_DB'\
                AND TABLE_NAME='patientComorbility' and column_name='type_comorb'"               
                cursorComorb.execute(sql_enum_comorb)                    
                #print(cursorComorb._last_executed)
                res=cursorComorb.fetchone()
                if res != None:
                    lst_comorb_enum=res['enume'].split(',')                    
                    
                for field in field_lst[1:]:
                   
                    field_values=field.split('=')  
                    if(field_values[0].replace(" ","").upper() in lst_comorb_enum):                        
                        #if I want to contraint the value                        
                        if(field_values[1] == '1'):
                            string_comorb=string_comorb+"'"+field_values[0].replace(" ","").upper()+"',"    
                            nb_comorb+=1
                        else: #if I dont want the value 0                             
                            string_not_comorb=string_not_comorb+"'"+field_values[0].replace(" ","").upper()+"',"    
                            nb_not_comorb+=1
                            
                        
                if nb_comorb>=1:
                    string_comorb=string_comorb[:-1]+") and value_comorb=1"   
                else: 
                    string_comorb=""
                dic_param['COMORB_YES']= string_comorb    
                
                if nb_not_comorb>=1:                   
                    string_not_comorb=string_not_comorb[:-1]+") and value_comorb=0"                       
                else: 
                    string_not_comorb=""
                dic_param['COMORB_NO']= string_not_comorb 
                    
                            
        if (field_lst[0].upper() == 'DRUGS'):
            if( len(field_lst) >= 2):
                string_drugs = " d.drug_name in ("
                nb_drugs=0
                string_not_drugs = " d.drug_name in ("
                nb_not_drugs=0
                sql_drug="select drug_name from drugs where drug_name= %s"            
                cursorDrug=connex.cursor()
                
                for field in field_lst[1:]:
                    field_values=field.split('=')            
                    cursorDrug.execute(sql_drug, [field_values[0].replace(" ","")])                    
                    res=cursorDrug.fetchone()
                    
                    if res!=None :
                        #if I want to contraint the value                        
                        if(field_values[1] == '1'):                            
                            
                            string_drugs = string_drugs+"'"+field_values[0].replace(" ","")+"',"                            
                            nb_drugs+=1  
                        else:                        
                            string_not_drugs = string_not_drugs+"'"+field_values[0].replace(" ","")+"',"
                            nb_not_drugs+=1  
                    elif (field_values[0].replace(" ","")!=""):
                        print("WARNING: "+ field_values[0] + ", this drug doesn't exist in database")
                        
                if nb_drugs>=1:                    
                    string_drugs = string_drugs[:-1]+")"        
                else:
                    string_drugs = ""
                dic_param['DRUGS_YES'] = string_drugs 
                if nb_not_drugs >= 1:
                    string_not_drugs = string_not_drugs[:-1]+")"        
                else:
                    string_not_drugs = ""
                dic_param['DRUGS_NO'] = string_not_drugs
                    
                cursorDrug.close()
    fd.close()    
    
   
    insert_patient_general(connex, string_age, string_gender, string_weight)
    insert_patient_drugs(connex, string_drugs, string_not_drugs,  nb_drugs, nb_not_drugs)
    insert_patient_comorbilities(connex, string_comorb, string_not_comorb,  nb_comorb, nb_not_comorb)
    return dic_param

def insert_patient_general(connex, string_age, string_gender, string_weight):
    """ Create a tmp table (tmp_patient2) using the data filtered by age, gender, weight
    notice the parameters are not mandatory
    
    Parameters
    ---------- 
    connex: database connection 
    string_age: the criteria of age to insert in the sql query
    string_gender: the criteria of the gender to insert in the sql query
    string_weight: the criteria of the weight to insert in the sql query
    """
    calculation.drop_table(connex, "tmp_patient2")
    create_tmp_patient(connex,"tmp_patient2")
    
    sql_insert="insert into tmp_patient2 select distinct(p.id_patient) from patient p "        
    sql_composed=""
    if(string_age !="" or string_gender!=""  or string_weight!=""):
        sql_composed= sql_insert +"where"
        if(string_age!=""):
            sql_composed= sql_composed + string_age +" AND "
        if(string_gender!=""):
            sql_composed= sql_composed + string_gender +" AND "
        if(string_weight!=""):
            sql_composed= sql_composed + string_weight +" AND "

        if(sql_composed[-1]==" "):
            sql_composed=sql_composed[:-4]     
    if(sql_composed==""): #if there are not criteria about the patients
        sql_composed=sql_insert
    
    try:       
        cursor=connex.cursor()
        cursor.execute(sql_composed)    
        connex.commit() 
        cursor.close()
        #I got the patient with basics conditions        
    except pymysql.Error as e:          
        logging.error('problem in insert_patient_general', e)     
        
def insert_patient_drugs(connex, string_drugs, string_not_drugs, nb_drugs, nb_not_drugs):
    """ Create a tmp table (tmp_patient2) using the data filtered by the drugs criteria
    
    Parameters
    ---------- 
    connex: database connection 
    string_drugs: the criteria of the drugs the patient takes
    string_not_drugs: the criteria of the drugs that the patient doesn't take
    nb_drugs: number of drugs taken
    nb_not_drugs number of drugs in the condition NOT drug
    """
    sql_insert2 = ""    
    join_drug_yes=""
    calculation.drop_table(connex, "tmp_drugs_yes")
    create_tmp_patient(connex,"tmp_drugs_yes")
    calculation.drop_table(connex, "tmp_drugs_no")
    create_tmp_patient(connex,"tmp_drugs_no")
    calculation.drop_table(connex, "tmp_patient3")
    create_tmp_patient(connex,"tmp_patient3")
    connex.commit()
    if(string_drugs != ""):
        sql_composed = "insert into tmp_drugs_yes  select distinct(p.id_patient) from tmp_patient2 p "\
                "inner join patientDrugs pd on (pd.id_patient=p.id_patient)"\
                "inner join drugs d on (d.id_drug=pd.id_drug)  WHERE "    
        sql_insert2 = sql_composed + string_drugs +" GROUP BY p.id_patient HAVING COUNT(*)='"+str(nb_drugs)+"'"
        try:
            cursor=connex.cursor()
            cursor.execute(sql_insert2)  
            cursor.close()        
        except pymysql.Error as e:          
            logging.error('problem in insert_patient_drugs', e)    
        connex.commit()
        join_drug_yes="inner join tmp_drugs_yes c on (c.id_patient=p.id_patient) "
        
    if(string_not_drugs != ""):
        sql_composed = "insert into tmp_drugs_no  select distinct(p.id_patient) from tmp_patient2 p "\
                "inner join patientDrugs pd on (pd.id_patient=p.id_patient)"\
                "inner join drugs d on (d.id_drug=pd.id_drug)  WHERE "  + string_not_drugs 
        
        #print(sql_composed)
        try:
            cursor=connex.cursor()
            cursor.execute(sql_composed)  
            cursor.close()
        
            sql_composed = "insert into tmp_patient3  select distinct(p.id_patient) from tmp_patient2 p "+\
                join_drug_yes + \
                " left join tmp_drugs_no n on (n.id_patient=p.id_patient) " +\
                " WHERE n.id_patient is null "     
            
            cursor=connex.cursor()
            cursor.execute(sql_composed)  
            #print(cursor._last_executed)
            cursor.close()
        
        except pymysql.Error as e:          
            logging.error('problem in insert_patient_comorbilities not comorb', e)     
    if(nb_not_drugs == 0):
        if nb_drugs >= 1:
            sql_insert3 = "insert into tmp_patient3 select id_patient from tmp_drugs_yes"
        else:
            sql_insert3 = "insert into tmp_patient3 select * from tmp_patient2"
        try:
            cursor = connex.cursor()
            cursor.execute(sql_insert3)  
            cursor.close()
        
        except pymysql.Error as e:          
            logging.error('problem in insert_patient_drugs end condition', e)     
        #print(cursor._last_executed)  
    
def insert_patient_comorbilities(connex, string_comorb, string_not_comorb, nb_comorb, nb_not_comorb):
    """ Create a tmp table (tmp_patient) using the data filtered by of comorb using the intermediate tables filtered by 
    general conditions and drugs
    
    Parameters
    ---------- 
    connex: database connection 
    string_comorb: the criteria of the comorbilities of the patient 
    string_not_comorb: the criteria of the comorbilities the patient doesnt have
    nb_comorb: number of comorb criteria
    nb_not_comorb number of comorb criteria to dont take in account
    """
    sql_insert2 = ""
    calculation.drop_table(connex, "tmp_comorb_yes")
    create_tmp_patient(connex,"tmp_comorb_yes")  
    join_comorb_yes=""
    if(string_comorb!=""):    
        sql_composed = "insert into tmp_comorb_yes  select distinct(p.id_patient) from tmp_patient3 p "\
                "inner join patientComorbility pc on (p.id_patient=pc.id_patient) where "             
        sql_insert2 = sql_composed + string_comorb +" GROUP BY p.id_patient HAVING COUNT(*)='"+str(nb_comorb)+"'"  
        print(sql_insert2)                       
        try:
            cursor=connex.cursor()
            cursor.execute(sql_insert2)  
            cursor.close()
        
        except pymysql.Error as e:          
            logging.error('problem in insert_patient_comorbilities', e)     
        #print(cursor._last_executed)
        connex.commit()
        join_comorb_yes=" inner join tmp_comorb_yes c on (c.id_patient=p.id_patient) "
        
    if(string_not_comorb!=""):
        sql_composed = "insert into tmp_patient  select distinct(p.id_patient) from tmp_patient3 p " +\
                join_comorb_yes +\
                "inner join patientComorbility pc on (p.id_patient=pc.id_patient) where "     
        sql_insert2 = sql_composed + string_not_comorb +" GROUP BY p.id_patient HAVING COUNT(*)='"+str(nb_comorb)+"'"

        try:
            cursor=connex.cursor()
            cursor.execute(sql_insert2)  
            cursor.close()
        
        except pymysql.Error as e:          
            logging.error('problem in insert_patient_comorbilities not comorb', e)     
        #print(cursor._last_executed)        
        
    if(nb_not_comorb==0):
        if nb_comorb>=1:
            sql_insert3="insert into tmp_patient select id_patient from tmp_comorb_yes"
        else:
            sql_insert3="insert into tmp_patient select * from tmp_patient3"
        try:
            cursor=connex.cursor()
            cursor.execute(sql_insert3)  
            cursor.close()
        
        except pymysql.Error as e:          
            logging.error('problem in insert_patient_comorbilities end condition', e)     
        
    connex.commit()
        
def add_population_criterias(connex):
    """ Put together the pseudo control frequencys and the variant frequencys
    
    Parameters
    ---------- 
    connex: database connection
    """
    #create table result
    print("add_population_criterias")
    cursor=connex.cursor() 
    sql_insert="INSERT INTO tmp_population_criteria (id_var, AC, AF, AN, HomoC, \
    AC_PseudoC, AF_PseudoC, AN_PseudoC, HomoC_PseudoC) \
    SELECT tmp_var.id_var, tmp_var.AC, tmp_var.AF, tmp_var.AN, tmp_var.HomoC, \
    tmp_pc.AC, tmp_pc.AF, tmp_pc.AN, tmp_pc.HomoC\
    FROM tmp_pc_variant_frequency tmp_pc\
    INNER JOIN tmp_variant_frequency tmp_var on tmp_var.id_var=tmp_pc.id_var"
    try:                 
        cursor.execute(sql_insert)
        connex.commit()
        return True
    except pymysql.Error as e:          
        logging.error('problem in add_disease_population')    
        logging.error(e)
    return False

def find_name_gr_disease(connex,lst_gr_diseases_id):
    """ Find the text name of the group disease of each element from the id lst 
    
    Parameters
    ---------- 
    connex: database connection
    lst_gr_diseases_id: the list of id_group diseases
    
    Return:
    -------
    a string containing the names of the lst passed in parameter separated by colons
    """
    string_lst_diseases=""
    string_short_names=""
    sql_select= "select short_name from groupDisease where id_groupe_disease in ("
    for id_gr_disease in lst_gr_diseases_id:
        string_lst_diseases=string_lst_diseases+"'"+str(id_gr_disease)+"',"    
    if(string_lst_diseases[-1]==","):
        string_lst_diseases=string_lst_diseases[:-1]
    try:        
        cursor=connex.cursor()
        sql_select= sql_select+ string_lst_diseases+ ")"
        cursor.execute(sql_select)
        res=cursor.fetchall()
        #print(cursor._last_executed)
        if res != None:
            for entry in res:
                string_short_names+=entry['short_name']+','
        if(string_short_names[-1]==','):
            string_short_names=string_short_names[:-1]
        return string_short_names
    except pymysql.Error as e:          
        logging.error('problem in add_disease_population')    
        logging.error(e)
    
    
def copy_population(connex, lst_diseases_id): 
    """ Copy the calculated population in the tmp_population_criteria table
    in the definive table
    
    Parameters
    ---------- 
    connex: database connection
    """
    txt_gr_diseases = find_name_gr_disease(connex, lst_diseases_id)
  
    
    sql_insert = "INSERT INTO alleleSubPopulationData (id_var,short_name_diseases, AC, AF, AN, HomoC, \
    AC_PseudoC, AF_PseudoC, AN_PseudoC, HomoC_PseudoC) \
    SELECT id_var,%s, AC, AF, AN, HomoC, \
    AC_PseudoC, AF_PseudoC, AN_PseudoC, HomoC_PseudoC FROM tmp_population_criteria"    
    try:                 
        cursor=connex.cursor() 
        cursor.execute(sql_insert, txt_gr_diseases)
        sql_primary_key="ALTER TABLE alleleSubPopulationData\
        ADD CONSTRAINT PK_alleleSubPopulationData PRIMARY KEY (`id_var`,`short_name_diseases`); "          
        cursor.execute(sql_primary_key)
        
        #FALTA UN DROP TABLE DE TMP
        
        connex.commit()
    except pymysql.Error as e:            
        logging.error('copy_tmp_def_population')    
        logging.error(e)


def export (file_export, dic_param):
    """ The fonction add the header with the criteria used, the txt will be add
    by bash
    
    Parameters
    ---------- 
    file_export: the file name to be written
    dic_param: the criterias of the treatment
    """
    fd= open(file_export, 'w')  
    fd.write("La extraccion comporta los siguientes criterios: \n")
    for param in dic_param:
        elem_value =dic_param[param]
        if (type(dic_param[param])==list):
            elem_value = ",".join(dic_param[param])            
        fd.write(param + ": " + elem_value + "\n")
    fd.close()
    
def calculate_frequency_criteria(connex, file_patient, file_disease,  pipeline, mode, file_out):
    """ The main procedure which takes the criterias in the input file parameters and generate
    table result
    
    Parameters
    ---------- 
    connex: database connection
    file_patient: the file containing the patient criteria
    file_disease: the file containing the disease criteria
    pipeline:SNV (if one day the CNV is added it's not still used )
    mode: DS the pseudo control will be calculate in the same disease group or default 
    which have the same behavior as in the population calc
    """
    
    print("calculate_frequency_criteria")
    #empty de final table     
    #Data stored in tmp_patient table
    
    dic_param=get_criteria_patient(connex, file_patient)
    lst_disease=get_criteria_disease(connex, file_disease, dic_param)  
    
    #empty the result table
    calculation.drop_table(connex, "alleleSubPopulationData");
    create_alleleSubPopulationData(connex);
    #empty the tmp table used to storage partial results
    drop_population_criteria(connex)
    create_population_criteria(connex)
    
    calculation.calculate_sample_condition_gr22(connex)
      
    #fill a tmp table with the samples which have the condition  
    #Attention DON'T USE 22 (VARIOS)          
    #calculate tmp_sample with specified conditions    
    txt_gr_disease= lst_disease_to_txt(lst_disease)
    calculation.drop_table(connex, "tmp_sample")
    calculation.create_tmp_sample_gen(connex, "tmp_sample")
    calculation.calculate_sample_condition(con=connex, lst_disease=txt_gr_disease, name_patient_table="tmp_patient", name_tmp_sample="tmp_sample")
    calculation.drop_table(connex, "tmp_sample_pc")
    calculation.create_tmp_sample_gen(connex, "tmp_sample_pc")
    connex.commit()
    calculation.drop_table(connex, "tmp_sample_default")
    calculation.create_tmp_sample_gen(connex, "tmp_sample_default")
    calculation.calculate_sample_condition(con=connex, lst_disease=txt_gr_disease, \
                                               name_patient_table="patient", name_tmp_sample="tmp_sample_default")
    connex.commit()
    if (mode.upper()=='DEFAULT'):   
        calculation.calculate_pseudo_control_condition(connex, "tmp_sample_default", "patient" )      
    elif (mode.upper()=='DS'):
        calculate_pseudo_control_GR(connex, dic_param, "tmp_sample_default", "tmp_patient2")      
      
    calculation.calculate_variant_frequency_disease(connex)
    calculation.calculate_pc_variant_frequency(connex)
    endT=False
    endT=add_population_criterias(connex)
         
    if (endT):              
        copy_population(connex, lst_disease)     
        
    export(file_out, dic_param)    
    
    print(dic_param)

def calculate_elim_patients(connex, name_patient_table, dic_param):
    """ In order to calculate the calculate the pseudocontrol we have composed a table tmp_sample_elim
    with the pacients who have at least one of the conditions required by the criterias
    
    Parameters
    ---------- 
    connex: database connection
    name_patient_table: the patient name table used as starting point
    dic_param: the criteria list in sql format for use purposes
 
    """
    print("calculate_pc_patients")        
    calculation.drop_table(connex, "tmp_drugs_no_or")
    create_tmp_patient(connex,"tmp_drugs_no_or") 
    calculation.drop_table(connex, "tmp_comorb_yes_or")
    create_tmp_patient(connex,"tmp_comorb_yes_or")
    calculation.drop_table(connex, "tmp_drugs_yes_or")
    create_tmp_patient(connex,"tmp_drugs_yes_or")
    calculation.drop_table(connex, "tmp_comorb_no_or")
    create_tmp_patient(connex,"tmp_comorb_no_or")     
    calculation.drop_table(connex, "tmp_patient_elim")
    create_tmp_patient(connex,"tmp_patient_elim")      
    calculation.drop_table(connex, "tmp_sample_elim")
    calculation.create_tmp_sample_gen(connex, "tmp_sample_elim")
    connex.commit()
    
    if (dic_param['DRUGS_YES'] !=""):
       
    
        sql_composed = "insert into tmp_drugs_yes_or  select distinct(p.id_patient) from "+name_patient_table+" p "+\
                "inner join patientDrugs pd on (pd.id_patient=p.id_patient)"\
                "inner join drugs d on (d.id_drug=pd.id_drug)  WHERE "  +  dic_param['DRUGS_YES']
        try:
            cursor=connex.cursor() 
            cursor.execute(sql_composed)
            cursor.close()
        except  pymysql.Error as e:           
            logging.error('calculate_pc_patients drugs_yes', e)  
        connex.commit()    

    if (dic_param['DRUGS_NO'] !=""):
        #table tmp drugs no contains all patient that have at least one drug  yes between the drugs unwanted
        
        calculation.drop_table(connex, "tmp_drugs_no_int")
        create_tmp_patient(connex,"tmp_drugs_no_int")
    
        sql_composed = "insert into tmp_drugs_no_int  select distinct(p.id_patient) from "+name_patient_table+" p "+\
                "inner join patientDrugs pd on (pd.id_patient=p.id_patient)"+\
                "inner join drugs d on (d.id_drug=pd.id_drug)  WHERE "  +  dic_param['DRUGS_NO']
        try:
            cursor=connex.cursor() 
            cursor.execute(sql_composed)
            cursor.close()
        except  pymysql.Error as e:           
            logging.error('calculate_pc_patients drugs_no', e)  
        connex.commit()    
        sql_composed = "insert into tmp_drugs_no_or  select distinct(p.id_patient) from "+name_patient_table+" p "+\
                "left join tmp_drugs_no_int tmp on (tmp.id_patient=p.id_patient)" +\
                " WHERE tmp.id_patient is null"

    if (dic_param['COMORB_YES'] !=""):

        sql_composed = "insert into tmp_comorb_yes_or  select distinct(p.id_patient) from "+\
                name_patient_table +" p "+\
                "inner join patientComorbility pc on (p.id_patient=pc.id_patient) where " + dic_param['COMORB_YES']
        try:
            cursor=connex.cursor() 
            cursor.execute(sql_composed)
            cursor.close()
        except  pymysql.Error as e:           
            logging.error('calculate_pc_patients drugs_yes', e)  
        connex.commit()   
        
    if (dic_param['COMORB_NO'] !=""):

        sql_composed = "insert into tmp_comorb_no_or  select distinct(p.id_patient) from "+ name_patient_table +" p "+\
                "inner join patientComorbility pc on (p.id_patient=pc.id_patient) where " + dic_param['COMORB_NO']
        try: 
            cursor=connex.cursor() 
            cursor.execute(sql_composed)
            cursor.close()
        except  pymysql.Error as e:           
            logging.error('calculate_pc_patients drugs_yes', e)  
        connex.commit()   

    sql_union = "insert into tmp_patient_elim select id_patient from tmp_drugs_no_or "+\
                "UNION select id_patient from tmp_drugs_yes_or "+\
                "UNION select id_patient from tmp_comorb_yes_or "+\
                "UNION select id_patient from tmp_comorb_no_or "
                
    #print (sql_union)
    try:
        cursor=connex.cursor()             
        cursor.execute(sql_union)
        cursor.close()
    except  pymysql.Error as e:           
        logging.error('calculate_pc_patients union', e)  
    
    sql_samples = "insert into tmp_sample_elim select distinct(id_sample) from tmp_patient_elim tmp "+\
                  "inner join sample s on (s.id_patient=tmp.id_patient)"    
    try:            
        cursor=connex.cursor() 
        cursor.execute(sql_samples)
        cursor.close()
    except  pymysql.Error as e:           
        logging.error('calculate_pc_patients tmp_sample_elim', e)  
    connex.commit()   
    calculation.drop_table(connex, "tmp_drugs_no_or")    
    calculation.drop_table(connex, "tmp_comorb_yes_or")
    calculation.drop_table(connex, "tmp_drugs_yes_or")
    calculation.drop_table(connex, "tmp_comorb_no_or")    
        
def calculate_pseudo_control_GR(connex, dic_param, name_tmp_sample_gr= "tmp_sample_default", name_tmp_patient="tmp_patient2" ):
    #precondition: tm_sample must be charged, dont need to know the condition
    #create table tmp de samples   
    """ Calculate the samples for the pseudo control 
    Parameters
    ---------- 
    connex: database connection   
    dic_param: the criteria list in sql format for use purposes
    name_tmp_sample_gr: name of the tmp_sample containing the samples of the pathologies selected
    name_tmp_patient: nom of the patient name or subset
    
    """
    print("calculate_pseudo_control_GR")
    calculate_elim_patients (connex, name_tmp_patient, dic_param)
    
    cursor=connex.cursor() 
    try:
        #every sample used in pc
        sql_insert= "insert into tmp_sample_pc(id_sample) " +\
        "SELECT distinct(s.id_sample) FROM "+name_tmp_sample_gr +" sd "+\
        "INNER JOIN sample s on (s.id_sample=sd.id_sample) " +\
        "INNER JOIN "+ name_tmp_patient +" p on (p.id_patient=s.id_patient) " + \
        "INNER JOIN familyPatient fp on (p.id_patient =fp.id_patient) "+\
        "LEFT JOIN tmp_sample_elim tmp_s on (tmp_s.id_sample=s.id_sample) " +\
        "LEFT JOIN tmp_sample_22 tmp_s2 on (tmp_s2.id_sample=s.id_sample) " +\
        "WHERE tmp_s.id_sample is null and tmp_s2.id_sample is null and fp.probandus=1"    

        cursor.execute(sql_insert)
        cursor.close()
       
    except  pymysql.Error as e:           
        logging.error('calculate_pseudo_control_disease', e)  
        
def drop_tables (connex, mode):
    calculation.drop_table(connex, "tmp_sample")
    calculation.drop_table(connex, "tmp_sample_pc")
    calculation.drop_table(connex, "tmp_sample_default")
    
    if (mode.upper()=='DS'):
        calculation.drop_table(connex, "tmp_patient2")            
    
log_name='db_log_patient.log'
db_curr=db.my_db()

patientlogger = logging.getLogger(log_name)
patientlogger.setLevel( getattr(logging, "ERROR") )

patientlogger.debug('Main')


if len(sys.argv) == 6:    
    file_patient = sys.argv[1]
    file_disease = sys.argv[2]
    file_dataConn = sys.argv[3]
    file_out = sys.argv[5]
    mode = sys.argv[4]
    
    dic_param=calculation.read_connexion(file_dataConn)

    if len(dic_param)!=0:
        if (db_curr.connect(dic_param["host"], dic_param["user"],\
                            dic_param["password"], dic_param["database"])==0): #connex ok     
            con=db_curr.get_connection()
            print(datetime.now())
            calculate_frequency_criteria(con, file_patient, file_disease, 'SNV', mode, file_out)
            print(datetime.now())
            drop_tables(con, mode)
        
    else:
        print("connex echec")    

     
else:
    patientlogger.error('Error - Se esperan 5 argumentos para realizar la carga')
    print('Error - Se esperan 5 argumentos para realizar la carga')
    print('Ejemplo: CriterioPacient.txt criteriodisease archivoConnexionDB DEFAULT ficheroexportname')


